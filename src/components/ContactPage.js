import React, { useState, useEffect } from 'react';
import { Form, Button, Container, Alert } from 'react-bootstrap';
import '../css/ContactPage.css';

const ContactPage = () => {
    const [formData, setFormData] = useState({
        email: '',
        title: '',
        message: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const [message, setMessage] = useState('');
    const [alertVariant, setAlertVariant] = useState('info');

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/send-contact-email`;
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user_email: formData.email,
                title: formData.title,
                message: formData.message,
            })
        })
            .then(async response => {
                const data = await response.json();
                if (!response.ok) {
                    setAlertVariant('danger');
                    setMessage(data.message || 'Có lỗi xảy ra, xin vui lòng thử lại.');
                    throw new Error(data.message || 'Có lỗi xảy ra khi gửi tin nhắn.');
                } else {
                    setAlertVariant('primary');
                    setMessage('Cám ơn những đóng góp của bạn! Đội ngũ Smart German Exam đã nhận được tin nhắn và sẽ sớm liên hệ lại với bạn.');
                }
            })
            .catch(error => {
                console.error('Fetch error:', error.message);
                setAlertVariant('danger');
                setMessage(error.message);
            });
        setSubmitted(true);
    };

    const handleBack = () => {
        setSubmitted(false);
        setFormData({
            email: '',
            title: '',
            message: ''
        });
    };

    return (
        <Container className="contact-container-wrap">
            <Container className={`contact-container ${submitted ? 'form-submitted' : ''}`}>
                <h1 className="contact-title">Liên hệ</h1>
                {submitted ? (
                    <>
                        <Alert variant={alertVariant} className="thank-you-message">
                            {message}
                        </Alert>
                        <div className="d-flex justify-content-end">
                            <Button variant="primary" onClick={handleBack} className="back-button">
                                Quay lại
                            </Button>
                        </div>
                    </>
                ) : (
                    <Form onSubmit={handleSubmit} className="contact-form">
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Địa chỉ E-mail của bạn</Form.Label>
                            <Form.Control type="email" className="email-form" placeholder="Nhập E-mail của bạn" name="email" value={formData.email} onChange={handleChange} />
                        </Form.Group>

                        <Form.Group controlId="formBasicTitle">
                            <Form.Label>Tiêu đề</Form.Label>
                            <Form.Control type="text" className="title-form" placeholder="Tiêu đề" name="title" value={formData.title} onChange={handleChange} />
                        </Form.Group>

                        <Form.Group controlId="formBasicMessage">
                            <Form.Label>Nội dung</Form.Label>
                            <Form.Control as="textarea" className="content-form" rows={3} name="message" value={formData.message} onChange={handleChange} />
                        </Form.Group>

                        <div className="d-flex justify-content-end">
                            <Button
                                variant="primary"
                                type="submit"
                                className="submit-button"
                                disabled={!formData.email || !formData.title || !formData.message}>
                                Gửi
                            </Button>
                        </div>
                    </Form>
                )}
            </Container>
        </Container>
    );
};

export default ContactPage;
