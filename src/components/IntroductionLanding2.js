import React from 'react'
import { Container, Col, Row } from 'react-bootstrap'
import "../css/IntroductionLanding2.css"



const IntroductionLanding2 = () => {

    return (
        <>
            <Container className="custom-container">
                <div style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
                    <Row className="w-100 my-4 custom-border" style={{
                        marginRight: 0,
                        marginLeft: 0,
                        backgroundColor: '#FFFFFF',
                        borderRadius: '25px',
                        padding: '0'
                    }}>
                        <Col>
                            <div className="p-5">
                                <h2 style={{
                                    fontWeight: 'bold' // This will apply bold styling to the h2 element
                                }}>
                                    Smart German Exam
                                </h2>
                                <p>
                                    Ứng dụng học tiếng Đức tích hợp AI được thiết kế riêng cho việc học tiếng Đức dành cho người Việt. Mang lại những phản hồi chính xác và cá nhân hoá.
                                </p>

                            </div>
                        </Col>
                    </Row>
                </div>
                <h2 style={{
                    fontWeight: 'bold', paddingBottom: '50px' // This will apply bold styling to the h2 element
                }}>Lí do mà Smart German Exam là sự lựa chọn phù hợp cho bạn</h2>
                <div style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
                    <Row className="w-100 my-4 row-style row-style" >
                        <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, borderRadius: '25px', padding: '0' }}>
                            <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>smart_toy</span>
                            </Col>
                            <Col xs={12} md={6}>
                                <div className="p-5">
                                    <h2 style={{ fontWeight: 'bold' }}>
                                        Đánh giá nhanh, chính xác với từng cá nhân
                                    </h2>
                                    <p>Lời nhận xét được thiết kế với AI trí tuệ nhân tạo đem lại sự chính xác và tiện lợi cho việc học tiếng Đức!</p>
                                </div>
                            </Col>
                        </Row>
                    </Row>
                    <Row className="w-100 my-4 second-row">
                        <Col xs={12} md={6}>
                            <div className="p-5">
                                <h2 style={{ fontWeight: 'bold' }}>
                                    Thực hành và làm bài tập mọi lúc mọi nơi
                                </h2>
                                <p>Bất kể bạn đang ở đâu và bất kì lúc nào. Smart German Exam luôn sẵn sàng giúp bạn!</p>
                            </div>
                        </Col>
                        <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>

                            <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>directions_run</span>
                        </Col>
                    </Row>

                    <Row className="w-100 my-4 row-style" >
                        <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, borderRadius: '25px', padding: '0' }}>
                            <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>sentiment_very_satisfied</span>
                            </Col>
                            <Col xs={12} md={6}>
                                <div className="p-5">
                                    <h2 style={{ fontWeight: 'bold' }}>
                                        Đồng hành cùng bạn làm việc học trở nên hấp dẫn
                                    </h2>
                                    <p>Đội ngũ Smart German Exam và các giáo viên có nhiều năm kinh nghiệm sẽ giúp bạn đạt được mục tiêu của mình!</p>
                                </div>
                            </Col>

                        </Row>
                    </Row>
                </div>
            </Container>
        </>
    )
}

export default IntroductionLanding2;