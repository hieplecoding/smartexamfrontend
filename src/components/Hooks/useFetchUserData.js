import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const useFetchUserData = () => {
  const [userData, setUserData] = useState(null);
  const [error, setError] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      navigate('/login');
      return;
    }
    const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/user-data`;
    fetch(apiUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      return response.json();
    })
    .then(data => {
      setUserData(data);
    })
    .catch(error => {
      console.error('Fetch error:', error.message);
      setError('Failed to load user data.');
    });
  }, [navigate]);

  return { userData, error };
};

export default useFetchUserData;
