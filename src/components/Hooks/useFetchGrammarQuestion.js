import { useState, useEffect } from 'react';

const useFetchGrammarQuestions = (navigate, topic) => {
    const [questions, setQuestions] = useState([]);

    useEffect(() => {
        // Early return if the topic is not selected (null, empty, or whatever your initial state is)
        if (!topic || topic === 'null') {
            setQuestions([]); // Optionally reset the questions state
            return;
        }

        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/show_grammar_questions?question_topic=${topic}`;
        fetch(apiUrl, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        .then(response => {
            if (response.status === 403) {
                // Handle subscription not active
                throw new Error('SubscriptionNotActive');
            } else if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => setQuestions(data))
        .catch(error => {
            if (error.message === 'SubscriptionNotActive') {
                navigate('/expired-subscription'); // Use the route you've defined for ExpiredSubscription
            }
            console.error('Fetch error:', error.message);
        });
    }, [navigate, topic]); // Add topic as a dependency to trigger the effect when it changes

    return questions;
};

export default useFetchGrammarQuestions;
