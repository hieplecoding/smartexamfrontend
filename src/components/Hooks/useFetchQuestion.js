import { useState, useEffect } from 'react';

const useFetchQuestions = (navigate) => {
    const [questions, setQuestions] = useState([]);
    

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/show_multiple_choice`;
        fetch(apiUrl, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                
                return response.json();
            })
            .then(data => setQuestions(data))
            .catch(error => {
                console.error('Fetch error:', error.message);
            });
    }, [navigate]);

    return questions;
};

export default useFetchQuestions;
