import { useState, createContext, useContext, useEffect } from 'react';
import { jwtDecode } from 'jwt-decode'; // Import jwt-decode to decode the JWT token

const AuthContext = createContext(null);

export const AuthProvider = ({ children }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const checkTokenExpiration = () => {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedToken = jwtDecode(token);
      const isExpired = decodedToken.exp * 1000 < Date.now();

      if (isExpired) {
        // If the token has expired, remove it and update the authentication state
        localStorage.removeItem('token');
        setIsAuthenticated(false);
      } else {
        // Token is not expired, ensure user is marked as authenticated
        setIsAuthenticated(true);
      }
    } else {
      // No token, user is not authenticated
      setIsAuthenticated(false);
    }
  };

  useEffect(() => {
    // Immediate check on load
    checkTokenExpiration();

    // Set up an interval for periodic checks
    const intervalId = setInterval(checkTokenExpiration, 60000); // Check every 60 seconds

    // Clean up interval on component unmount
    return () => clearInterval(intervalId);
  }, []);

  const login = (token) => {
    localStorage.setItem('token', token);
    setIsAuthenticated(true);
  };

  const logout = () => {
    localStorage.removeItem('token');
    setIsAuthenticated(false);
  };

   // Function to get the token from local storage
  const getToken = () => {
    return localStorage.getItem('token');
  };

  return (
    <AuthContext.Provider value={{ isAuthenticated, login, logout, getToken}}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);