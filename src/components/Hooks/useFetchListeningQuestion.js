import { useState, useEffect } from 'react';



const useFetchListeningQuestions = (service, level, navigate) => {
    const [questions, setQuestions] = useState([]);
    

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/show_listening_question?level=${level}`;
        fetch(apiUrl, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => {
                if (response.status === 403) {
                    // Handle subscription not active
                    throw new Error('SubscriptionNotActive');
                } else if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => setQuestions(data))
            .catch(error => {
                if (error.message === 'SubscriptionNotActive') {
                    navigate('/expired-subscription'); // Use the route you've defined for ExpiredSubscription
                }
                console.error('Fetch error:', error.message);
            });
    }, [service, level, navigate]);

    return questions;
};

export default useFetchListeningQuestions;
