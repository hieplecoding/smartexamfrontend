import { useState } from 'react';

const useDoSpeaking = (navigate) => {
    const [audioLink, setAudioLink] = useState(null);
    const [error, setError] = useState(null);

    const doSpeaking = async (audioBlob) => {
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const threadId = localStorage.getItem('threadId');
        if (!threadId) {
            setError('Thread ID is missing');
            return;
        }

        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/speaking?thread_id=${threadId}`;
        const formData = new FormData();
        formData.append('audio_file', audioBlob, 'userSpeech.wav');
        console.log(formData)
        // Correct way to log FormData contents
        for (let [key, value] of formData.entries()) {
            console.log(`${key}: ${value}`);  // This will log keys and values of the formData
        }

        try {
            const response = await fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                    
                    // Do not set 'Content-Type': 'multipart/form-data' here,
                    // as it should be set automatically by the browser along with the correct boundary.
                },
                body: formData
            });
            if (response.status === 403) {
                throw new Error('SubscriptionNotActive');
            } else if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            const data = await response.json();
            setAudioLink(data.response);
        } catch (error) {
            setError(error.message);
            console.error('Fetch error:', error.message);
        }
    };

    return { audioLink, error, doSpeaking };
};

export default useDoSpeaking;