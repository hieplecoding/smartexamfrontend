import { useState } from 'react';

const useSpeakingThreadId = (navigate) => {
    const [threadId, setThreadId] = useState(null);
    const [error, setError] = useState(null);

    const fetchThreadId = () => {
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/create_thread_speaking`;

        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        .then(response => {
            console.log("Response:", response);
            if (response.status === 403) {
                throw new Error('SubscriptionNotActive');
            } else if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            console.log("Data received:", data);
            localStorage.setItem('threadId', data.response);
            setThreadId(data.response);
        })
        .catch(error => {
            if (error.message === 'SubscriptionNotActive') {
                navigate('/expired-subscription');
            }
            setError(error.message);
            console.error('Fetch error:', error.message);
        });
    };

    return { threadId, error, fetchThreadId };
};

export default useSpeakingThreadId;
