import React from 'react';
import useScreenWidth from '../components/Hooks/useScreenWidth';
import TeamPage from './TeamPage';
import TeamPageMobile from './TeamPageMobile';

const TeamPageWrapper = () => {
    const screenWidth = useScreenWidth();

    return (
        <>
            {screenWidth > 991 ? <TeamPage /> : <TeamPageMobile />}
        </>
    );
};

export default TeamPageWrapper;
