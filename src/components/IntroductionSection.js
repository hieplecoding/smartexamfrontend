import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Row, Col } from 'react-bootstrap';
import '../App.css';
import '../css/IntroductionLandingPage.css';

const IntroductionSection = () => {
  return (
    <div className="intro-fullpage">
      <div className="intro-blur-overlay"></div>
      <div className="background-image"></div>
      <Row >
        <Col md={6}>
          <h2>Chào mừng các bạn đến với Smart German Exam</h2>
          <p>Hãy cùng bắt đầu với chúng tôi để có thể tự tin sử dụng tiếng Đức. Bài tập dành cho mọi cấp độ, luyện tập về khả năng viết, nghe, ngữ pháp.</p>
          <Link to="/register">
            <Button variant="btn btn-success" size="lg">Bắt đầu</Button>
          </Link>
          <div className="my-3">
            <span className="mr-2">Đánh giá tốt từ nhiều người sử dụng</span>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default IntroductionSection;
