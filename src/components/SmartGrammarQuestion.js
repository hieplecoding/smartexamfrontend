import React, { useState, useRef, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import useFetchGrammarQuestions from './Hooks/useFetchGrammarQuestion';
import GrammarQuestion from './utilities/GrammarQuestion';
import image from '../photos/Picture 1.webp';
import '../App.css';
import '../css/PagingInGrammarAndListening.css'
import '../css/FloatingSidebar.css'
import '../css/IntroductionSectionInAll.css';
import '../css/SmartGrammar.css';

// Helper function to shuffle questions
function shuffle(array) {
  let currentIndex = array.length, randomIndex;
  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }
  return array;
}

const SmartGrammarQuestion = () => {
  const navigate = useNavigate();
  const introRef = useRef(null);
  const questionsRef = useRef(null);
  const sidebarRef = useRef(null);

  const [selectedTopic, setSelectedTopic] = useState('');
  // Directly fetch and store questions
  const fetchedQuestions = useFetchGrammarQuestions(navigate, selectedTopic);

  const [questions, setQuestions] = useState([]);
  const [answeredQuestions, setAnsweredQuestions] = useState(new Set());
  const [correctAnswers, setCorrectAnswers] = useState(0);
  const [activePopupId, setActivePopupId] = useState(null);
  const [sidebarVisible, setSidebarVisible] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [questionsPerPage] = useState(5);

  useEffect(() => {
    // Shuffle questions and set state once on component mount and when selectedTopic changes
    setQuestions(shuffle([...fetchedQuestions]));
  }, [fetchedQuestions]); // This effect depends on fetchedQuestions

  const handleCorrectAnswer = (questionId) => {
    if (!answeredQuestions.has(questionId)) {
      setAnsweredQuestions(new Set(answeredQuestions.add(questionId)));
      setCorrectAnswers(correctAnswers + 1);
    }
  };

  const handlePopupToggle = (id) => {
    setActivePopupId(prevId => prevId !== id ? id : null);
  };

  const handleTopicSelection = (event) => {
    const topic = event.target.value;
    setSelectedTopic(topic === 'null' ? null : topic);
  };

  // Calculate the current questions to display
  const indexOfLastQuestion = currentPage * questionsPerPage;
  const indexOfFirstQuestion = indexOfLastQuestion - questionsPerPage;
  const currentQuestions = questions.slice(indexOfFirstQuestion, indexOfLastQuestion);

  // Calculate page numbers
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(questions.length / questionsPerPage); i++) {
    pageNumbers.push(i);
  }

  const setCurrentPageAndScrollToSection = (pageNumber) => {
    setCurrentPage(pageNumber);
    if (questionsRef.current) {
      questionsRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      // Get the bottom position of the intro section
      const introBottom = introRef.current?.getBoundingClientRect().bottom + window.scrollY;
      // Define how far past the intro section the sidebar should start appearing
      const sidebarAppearanceThreshold = introBottom + 110;

      // Determine if the sidebar should be visible
      const shouldShowSidebar = window.scrollY > sidebarAppearanceThreshold;
      const isMobileView = window.innerWidth <= 905;

      // The sidebar is visible if we're not in mobile view and the scroll position is past the threshold
      setSidebarVisible(shouldShowSidebar && !isMobileView);
    };

    // Add scroll and resize event listeners
    window.addEventListener('scroll', handleScroll);
    window.addEventListener('resize', handleScroll);

    // Call it on mount to initialize the sidebar state correctly
    handleScroll();

    // Clean up event listeners on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleScroll);
    };
  }, []);

  return (
    <>
      {/* Floating Sidebar */}
      <div ref={sidebarRef} className={`floating-sidebar ${sidebarVisible ? 'visible' : ''}`}>
        <div className="sidebar-content">
          <div className="correct-answers-badge" style={{ marginBottom: '15px' }}>
            <span className="badge-text">Câu trả lời đúng: </span>
            <span className="badge-count">{correctAnswers}</span>
          </div>
          <div className="scroll-icon" onClick={() => questionsRef.current?.scrollIntoView({ behavior: 'smooth', block: 'start' })}>
            △
          </div>
          <div className="scroll-icon" onClick={() => window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })}>
            ▽
          </div>
        </div>
      </div>

      {/* Introduction Section */}
      <div ref={introRef} className="intro-section">
        <div className="container">
          <div className="row align-items-center">
            {/* Text Content on the Left */}
            <div className="col-12 col-md-6 intro-text-content" >
              <h2>Smart Grammar</h2>
              <p>Tại đây các bạn sẽ được ôn tập các chủ đề bằng các câu hỏi trắc nghiệm giúp củng cố kiến thức cũng như ghi nhớ tốt hơn.</p>
            </div>
            {/* Image on the Right */}
            <div className="col-12 col-md-6 intro-image-content">
              <img src={image} alt="Introduction" className="intro-image img-fluid" />
            </div>
          </div>
        </div>
      </div>

      <div ref={questionsRef} className="container my-5">
        <h4 className="text-center mb-4 choose-topic-text">Hãy chọn một chủ đề để bắt đầu</h4>
        <div className="text-center mb-4">
          {/* Floating arrow container */}
          <div className="arrow-container">
            <span className="arrow-down">👇</span>
          </div>
          <select className="form-select" aria-label="Grammar topic selection" onChange={handleTopicSelection} value={selectedTopic || 'null'}>
            <option value="null">Chọn chủ đề</option>
            <option value="nominativ">Nominativ</option>
            <option value="genitiv">Genitiv</option>
            <option value="dativ">Dativ</option>
            <option value="akkusativ">Akkusativ</option>
            <option value="bestimmterArtikel">bestimmter Artikel</option>
            <option value="akjektiveBestimmterArtikel">Adjektiv mit bestimmten Artikel</option>
            <option value="unbestimmterArtikel">unbestimmter Artikel</option>
            <option value="relativSatz">RelativSatz</option>
            <option value="PossessivartikelDativ">Possessivartikel Dativ</option>
            <option value="PossessivartikelAkkusativ">Possessivartikel Akkusativ</option>
            <option value="null">Xoá</option>
          </select>
        </div>

        <div className="row">
          <div className="col-12 col-md-8 col-lg-6 mx-auto">
            {Array.isArray(currentQuestions) && currentQuestions.map(question => (
              <div key={question.id_question}>
                <GrammarQuestion
                  question={question}
                  onCorrectAnswer={() => handleCorrectAnswer(question.id_question)}
                  onPopupToggle={() => handlePopupToggle(question.id_question)}
                  isPopupActive={activePopupId === question.id_question}
                  setActivePopupId={setActivePopupId}
                  isLocked={answeredQuestions.has(question.id_question)}
                />
              </div>
            ))}
          </div>
        </div>

        {/* Pagination controls */}
        <div className="pagination" style={{ marginTop: '40px' }}>
          {pageNumbers.map(number => (
            <button
              key={number}
              onClick={() => setCurrentPageAndScrollToSection(number)}
              className={`page-item ${currentPage === number ? 'active' : ''}`}>
              {number}
            </button>
          ))}
        </div>

        {!sidebarVisible && selectedTopic && (
          <div className="mobile-correct-answers">
            <div className="correct-answers-badge">
              <span className="badge-text">Câu trả lời đúng: </span>
              <span className="badge-count">{correctAnswers}</span>
            </div>
          </div>
        )}

      </div>
    </>
  );
};

export default SmartGrammarQuestion;
