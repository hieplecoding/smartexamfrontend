import React, { useState, useRef, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import useFetchListeningQuestions from './Hooks/useFetchListeningQuestion';
import ListeningInput from './utilities/ListeningInput';
import image from '../photos/Picture 2.webp';
import '../App.css';
import '../css/PagingInGrammarAndListening.css'
import '../css/FloatingSidebar.css'
import '../css/IntroductionSectionInAll.css';
import '../css/LevelButton.css';

// Helper function to shuffle questions
function shuffle(array) {
  let currentIndex = array.length, randomIndex;
  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }
  return array;
}

const SmartListening = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [selectedLevel, setSelectedLevel] = useState('');
  const { service, level } = location.state || { service: 'defaultService', level: '' };

  // Fetch questions and store them
  const fetchedQuestions = useFetchListeningQuestions(service, level, navigate);

  // State to hold the shuffled questions
  const [questions, setQuestions] = useState([]);

  // Set to keep track of answered question states
  const [answeredQuestions, setAnsweredQuestions] = useState({});

  // Shuffle questions once when they are fetched
  useEffect(() => {
    if (fetchedQuestions.length > 0) {
      setQuestions(shuffle([...fetchedQuestions]));
    }
  }, [fetchedQuestions]);

  const introRef = useRef(null);
  const questionsRef = useRef(null);
  const sidebarRef = useRef(null);

  const [correctAnswers, setCorrectAnswers] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [questionsPerPage] = useState(5);
  const [sidebarVisible, setSidebarVisible] = useState(false);

  const handleAnswerChange = (questionId, isCorrect, userAnswer) => {
    const newAnsweredQuestions = { ...answeredQuestions };

    // Update state with locked status, correctness, and user's answer
    newAnsweredQuestions[questionId] = {
      isCorrect: isCorrect,
      locked: isCorrect,
      userAnswer: userAnswer
    };

    setAnsweredQuestions(newAnsweredQuestions);

    // Adjust correct answers count based on the correctness of the answer
    if (isCorrect && !answeredQuestions[questionId]?.isCorrect) {
      setCorrectAnswers(correctAnswers + 1);
    } else if (!isCorrect && answeredQuestions[questionId]?.isCorrect) {
      setCorrectAnswers(correctAnswers - 1);
    }
  };

  const setCurrentPageAndScrollToSection = (pageNumber) => {
    setCurrentPage(pageNumber);
    if (questionsRef.current) {
      questionsRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    }
  };

  const indexOfLastQuestion = currentPage * questionsPerPage;
  const indexOfFirstQuestion = indexOfLastQuestion - questionsPerPage;
  const currentQuestions = questions.slice(indexOfFirstQuestion, indexOfLastQuestion);

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(questions.length / questionsPerPage); i++) {
    pageNumbers.push(i);
  }

  const handleLevelSelection = (selectedLevel) => {
    // Check if the selected level is A2, B1, or B2
    if (['A2', 'B1', 'B2'].includes(selectedLevel)) {
      // If so, navigate to the ComingSoon component
      navigate('/coming-soon');
    } else {
      // Otherwise, navigate to the listening level with the selected level
      navigate('/listening-level', { state: { level: selectedLevel } });
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      // Get the bottom position of the intro section
      const introBottom = introRef.current?.getBoundingClientRect().bottom + window.scrollY;
      // Define how far past the intro section the sidebar should start appearing
      const sidebarAppearanceThreshold = introBottom - 150;

      // Determine if the sidebar should be visible
      const shouldShowSidebar = window.scrollY > sidebarAppearanceThreshold;
      const isMobileView = window.innerWidth <= 905;

      // The sidebar is visible if we're not in mobile view and the scroll position is past the threshold
      setSidebarVisible(shouldShowSidebar && !isMobileView);
    };

    // Add scroll and resize event listeners
    window.addEventListener('scroll', handleScroll);
    window.addEventListener('resize', handleScroll);

    // Call it on mount to initialize the sidebar state correctly
    handleScroll();

    // Directly handle the selection effect if `level` is provided
    if (level) {
      setSelectedLevel(level);
      // Optional: Insert any additional actions you want to take when a level is pre-selected
    }

    // Clean up event listeners on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleScroll);
    };
  }, [level]);

  return (
    <>
      {/* Floating Sidebar */}
      <div ref={sidebarRef} className={`floating-sidebar ${sidebarVisible ? 'visible' : ''}`}>
        <div className="sidebar-content">
          <div className="correct-answers-badge" style={{ marginBottom: '15px' }}>
            <span className="badge-text">Câu trả lời đúng: </span>
            <span className="badge-count">{correctAnswers}</span>
          </div>
          <div className="level-buttons">
            {['A1', 'A2', 'B1', 'B2'].map((level) => (
              <button
                key={level}
                className={`level-button ${selectedLevel === level ? 'selected' : ''}`}
                onClick={() => handleLevelSelection(level)}
              >
                {level}
              </button>
            ))}
          </div>
          <div className="scroll-icon" onClick={() => questionsRef.current?.scrollIntoView({ behavior: 'smooth', block: 'start' })}>
            △
          </div>
          <div className="scroll-icon" onClick={() => window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })}>
            ▽
          </div>
        </div>
      </div>

      {/* Introduction Section */}
      <div ref={introRef} className="intro-section">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-12 col-md-6 intro-text-content">
              <h2>Smart Listening</h2>
              <p>Tại đây các bạn sẽ được luyện tập kỹ năng nghe qua các câu hỏi trắc nghiệm được sắp xếp phù hợp theo trình độ bởi đội ngũ Smart Exam German.</p>
            </div>
            <div className="col-12 col-md-6 intro-image-content">
              <img src={image} alt="Introduction" className="intro-image img-fluid" style={{ maxHeight: '550px', marginRight: '100px' }} />
            </div>
          </div>
        </div>
      </div>

      <div ref={questionsRef} className="container my-1">
        <div className="row">
          <div className="col-12 col-md-8 col-lg-6 mx-auto">
            {Array.isArray(currentQuestions) && currentQuestions.map(question => (
              <div key={question.id_question}>
                <ListeningInput
                  question={question}
                  onCorrectAnswer={() => handleAnswerChange(question.id_question, true, answeredQuestions[question.id_question]?.userAnswer)}
                  onIncorrectAnswer={() => handleAnswerChange(question.id_question, false, answeredQuestions[question.id_question]?.userAnswer)}
                  locked={answeredQuestions[question.id_question]?.locked}
                  userAnswer={answeredQuestions[question.id_question]?.userAnswer} // Extracting userAnswer from the state
                />
              </div>
            ))}

          </div>
        </div>

        {/* Pagination controls */}
        <div className="pagination">
          {pageNumbers.map(number => (
            <button
              key={number}
              onClick={() => setCurrentPageAndScrollToSection(number)}
              className={`page-item ${currentPage === number ? 'active' : ''}`}>
              {number}
            </button>
          ))}
        </div>

        {!sidebarVisible && (
          <div className="mobile-correct-answers">
            <div className="correct-answers-badge">
              <span className="badge-text">Câu trả lời đúng: </span>
              <span className="badge-count">{correctAnswers}</span>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default SmartListening;