import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { useAuth } from './Hooks/useAuth';
import { Form, Button, Alert, Container, InputGroup, Card, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import { useGoogleLogin } from '@react-oauth/google';

const Login = () => {
  const [identifier, setIdentifier] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false); // New state for password visibility
  const [loginMessage, setLoginMessage] = useState({ text: '', type: '' }); // State for login message
  const [resetPasswordMessage, setResetPasswordMessage] = useState({ text: '', type: '' }); // State for reset password message
  const navigate = useNavigate();
  const location = useLocation();
  const auth = useAuth();
  const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/login`;
  const [showForgotPasswordModal, setShowForgotPasswordModal] = useState(false);
  const [resetEmailOrUsername, setResetEmailOrUsername] = useState('');

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/landing-2');
    } else {
      navigate('/login');
    }
  }, [navigate]);

  const handleLogin = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ identifier, password }),
      });

      const data = await response.json();

      if (!response.ok) {
        const errorMessage = data.detail;
        throw new Error(errorMessage);
      }

      if (data.access_token) {
        auth.login(data.access_token);
        localStorage.setItem('token', data.access_token);
        setLoginMessage({ text: data.message });

        const redirectPath = new URLSearchParams(location.search).get('redirect') || '/landing-2';
        navigate(redirectPath);
      } else {
        throw new Error('Token not provided');
      }
    } catch (error) {
      console.error('Login error:', error);
      setLoginMessage({ text: error.message });
    }
    setResetPasswordMessage({ text: '', type: '' });
  };


  // Function to toggle the password visibility
  const toggleShowPassword = () => setShowPassword(!showPassword);

  const handleForgotPasswordSubmit = async () => {
    const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/forgot-password`;

    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username_or_email: resetEmailOrUsername }),
      });

      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.message);
      }

      const messageType = data.actionTaken ? 'primary' : 'danger';
      setResetPasswordMessage({ text: data.message, type: messageType });
    } catch (error) {
      console.error('Failed to send forgot password request:', error);
      setResetPasswordMessage({ text: error.toString(), type: 'danger' });
    }
  };
  // const googleLogin = useGoogleLogin({
  //   onSuccess: async (codeResponse) => {
  //     const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/google-auth`;

  //     try {
  //       const response = await fetch(apiUrl, {
  //         method: 'POST',
  //         headers: {
  //           'Content-Type': 'application/json',
  //         },
  //         body: JSON.stringify({ access_token: codeResponse.access_token }), // Adjust according to what your backend expects
  //       });

  //       if (!response.ok) {
  //         throw new Error('Failed to authenticate with the backend');
  //       }

  //       const data = await response.json();
  //       if (data.access_token) {
  //         // Store the token and update authentication state
  //         localStorage.setItem('token', data.access_token);
  //         auth.login(data.access_token); // Assuming your useAuth hook has a login method for updating auth state
  //         setMessage({ text: 'Login successful!', type: 'success' });

  //         const redirectPath = location.state?.from?.pathname || '/landing-2';
  //         navigate(redirectPath);
  //       } else {
  //         throw new Error('Token not provided');
  //       }
  //     } catch (error) {
  //       console.error('Error during backend authentication:', error);
  //       setMessage({ text: error.message, type: 'error' });
  //     }
  //   },
  //   onError: (error) => {
  //     console.log('Google login error:', error);
  //     setMessage({ text: error.error, type: 'error' });
  //   },
  // });

  const openForgotPasswordModal = () => {
    setShowForgotPasswordModal(true);
    setLoginMessage({ text: '', type: '' }); // Clear login message when opening the modal
  };

  const closeForgotPasswordModal = () => {
    setShowForgotPasswordModal(false);
    setResetPasswordMessage({ text: '', type: '' }); // Clear reset password message when closing the modal
  };

  return (
    <Container className="d-flex align-items-center justify-content-center" style={{ minHeight: "80vh" }}>
      <Card className="w-100" style={{ maxWidth: "400px" }}>
        <Card.Body>
          <h2 className="text-center mb-4">Đăng nhập</h2>
          <Form onSubmit={handleLogin}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                placeholder="Tên đăng nhập hoặc Email"
                value={identifier}
                onChange={(e) => setIdentifier(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <InputGroup>
                <Form.Control
                  type={showPassword ? "text" : "password"}
                  placeholder="Mật khẩu"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <Button variant="outline-secondary" onClick={toggleShowPassword}>
                  {showPassword ? 'Ẩn' : 'Hiển thị'}
                </Button>
              </InputGroup>
            </Form.Group>

            <Button variant="secondary" type="submit" className="w-100 mb-3">
              Đăng nhập
            </Button>
            <div className="mb-3">
              <Link to="/register">
                <Button variant="primary" className="w-100 mt-2">
                  Tạo tài khoản
                </Button>
              </Link>
            </div>
            <div className="text-end">
              <Button variant="link" onClick={openForgotPasswordModal} style={{ textDecoration: 'underline', padding: 0, border: 'none', backgroundColor: 'transparent' }}>
                Bạn quên mật khẩu?
              </Button>
            </div>
          </Form>
          {/* <Button variant="light" className="w-100 mt-3" onClick={() => googleLogin()}>
            <i className="fa-brands fa-google" style={{ marginRight: '10px' }}></i>
            Đăng nhập với Google
          </Button> */}

          {!showForgotPasswordModal && loginMessage.text && (
            <Alert variant={loginMessage.type === 'success' ? 'success' : 'danger'} className="mt-2">
              {loginMessage.text}
            </Alert>
          )}
        </Card.Body>
      </Card>

      {/* Forgot Password Modal */}
      <Modal show={showForgotPasswordModal} onHide={closeForgotPasswordModal}>
        <Modal.Header closeButton>
          <Modal.Title>Đặt lại mật khẩu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Nhập email hoặc tên người dùng của bạn</Form.Label>
              <div className='mt-2'>
                <Form.Control
                  type="text"
                  placeholder="Email hoặc tên đăng nhập"
                  value={resetEmailOrUsername}
                  onChange={(e) => setResetEmailOrUsername(e.target.value)}
                />
              </div>
            </Form.Group>
          </Form>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowForgotPasswordModal(false)}>
            Đóng
          </Button>
          <Button variant="primary" onClick={handleForgotPasswordSubmit}>
            Gửi yêu cầu
          </Button>
        </Modal.Footer>
        {showForgotPasswordModal && resetPasswordMessage.text && (
          <Alert variant={resetPasswordMessage.type} className="mt-2" style={{ marginLeft: '15px', marginRight: '15px', marginBottom: '15px' }}>
            {resetPasswordMessage.text}
          </Alert>
        )}
      </Modal>
    </Container>
  );
};

export default Login;

