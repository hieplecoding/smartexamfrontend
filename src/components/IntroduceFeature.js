import React from 'react';
import { Card, Row, Col, Container } from 'react-bootstrap';
import Pic1 from '../photos/EssayLandingPage.webp';
import Pic2 from '../photos/ListeningLandingPage.webp';
import Pic3 from '../photos/GrammarLandingPage.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faHeadphones, faBook } from '@fortawesome/free-solid-svg-icons';
import '../css/IntroduceFeatureInLandingPage.css';

const IntroduceFeature = () => {
    const features = [
        {
            title: "Smart Essay",
            icon: 'edit',
            description: 'Tại đây các bạn sẽ được luyện tập khả năng viết thông qua sự hỗ trợ của AI trong việc sửa ngữ pháp cũng như đề xuất các ý tưởng.',
            imgSrc: Pic1,
        },
        {
            title: "Smart Listening",
            icon: 'headphones',
            description: 'Tại đây các bạn sẽ được luyện tập kỹ năng nghe qua các câu hỏi trắc nghiệm được sắp xếp phù hợp theo trình độ bởi đội ngũ Smart Exam German.',
            imgSrc: Pic2,
        },
        {
            title: "Smart Grammar",
            icon: 'book',
            description: 'Tại đây các bạn sẽ được ôn tập các chủ đề bằng các câu hỏi trắc nghiệm giúp củng cố kiến thức cũng như ghi nhớ tốt hơn.',
            imgSrc: Pic3,
        },
    ];

    const iconStyles = { fontSize: '30px', color: 'black', borderRadius: '5px', marginRight: '6px', marginBottom: '1px' };

    const renderIcon = (icon) => {
        let iconToRender;
        switch (icon) {
            case 'edit':
                iconToRender = faEdit;
                break;
            case 'headphones':
                iconToRender = faHeadphones;
                break;
            case 'book':
                iconToRender = faBook;
                break
            default:
                iconToRender = null;
        }
        return <FontAwesomeIcon icon={iconToRender} style={iconStyles} />;
    };


    return (
        <Container className="custom-container">
            <Row className="justify-content-center mx-0" style={{ marginBottom: '20px' }}>
                {features.map((feature, idx) => (
                    <Col key={idx} lg={4} md={6} sm={12} className="p-2">
                        <Card className="feature-card text-center h-100">
                            <Card.Img variant="top" src={feature.imgSrc} />
                            <Card.Body>
                                <i className="material-icons" style={{ marginRight: '8px' }}>{renderIcon(feature.icon)}</i>
                                <span className="outlined-text">{feature.title}</span>
                                <Card.Text>{feature.description}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>
    );
};

export default IntroduceFeature;
