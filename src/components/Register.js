import React, { useState, useEffect } from 'react';
import { Form, Button, Alert, Container, InputGroup, Card } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordValid, setPasswordValid] = useState(false); // State to track password validity
  const [showPassword, setShowPassword] = useState(false);
  const [user_email, setUserEmail] = useState('');
  const [message, setMessage] = useState('');
  const [alertVariant, setAlertVariant] = useState('info');
  const navigate = useNavigate();

  const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/register`;

  // Password strength validation function
  useEffect(() => {
    const strongPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})');
    setPasswordValid(strongPassword.test(password));
  }, [password]);

  const handleRegister = async (event) => {
    event.preventDefault();
    if (!passwordValid) {
      setMessage('Mật khẩu phải dài ít nhất 8 ký tự và bao gồm ít nhất một chữ hoa, một chữ thường, một số và một ký tự đặc biệt!');
      setAlertVariant('danger');
      return; // Prevent form submission if the password is not valid
    }

    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password, user_email }),
      });

      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.detail);
      }

      setMessage(data.registration);
      setAlertVariant('primary');
      navigate('/verify-email', { state: { message: "Bạn đã đăng ký thành công! Chúng tôi đã gửi một link xác nhận đến Email của bạn. Vui lòng kiểm tra Email và xác nhận." } });
    } catch (error) {
      setMessage(error.message);
      setAlertVariant('danger');
    }
  };

  // Function to toggle the password visibility
  const toggleShowPassword = () => setShowPassword(!showPassword);

  return (
    <Container className="d-flex align-items-center justify-content-center" style={{ minHeight: "80vh" }}>
      <Card className="w-100" style={{ maxWidth: "400px" }}>
        <Card.Body>
          <h2 className="text-center mb-4">Đăng ký</h2>
          <Form onSubmit={handleRegister}>
            <Form.Group className="mb-3" controlId="formBasicUsername">
              <Form.Control
                type="text"
                placeholder="Tên đăng nhập"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <InputGroup>
                <Form.Control
                  type={showPassword ? "text" : "password"}
                  placeholder="Mật khẩu"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  isInvalid={!passwordValid && password.length > 0}
                />
                <Button variant="outline-secondary" onClick={toggleShowPassword}>
                  {showPassword ? 'Ẩn' : 'Hiển thị'}
                </Button>
                <Form.Control.Feedback type="invalid">
                  Mật khẩu phải dài ít nhất 8 ký tự và bao gồm ít nhất một chữ hoa, một chữ thường, một số và một ký tự đặc biệt.
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Email"
                value={user_email}
                onChange={(e) => setUserEmail(e.target.value)}
              />
            </Form.Group>

            <Button variant="secondary" type="submit" className="w-100" disabled={!passwordValid}>
              Đăng ký
            </Button>
          </Form>

          {message && (
            <Alert variant={alertVariant} className="mt-3">
              {message}
            </Alert>
          )}
        </Card.Body>
      </Card>
    </Container>
  );
};

export default Register;
