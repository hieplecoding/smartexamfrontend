import React from 'react';
import { Row, Col , Container} from 'react-bootstrap';
import '../css/ExpiredSubscription.css'
const ExpiredSubscription = () => {


    return (
        <>
            <Container>
                <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, backgroundColor: '#FFFFFF', borderRadius: '25px', padding: '0' }}>
                    <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>sentiment_dissatisfied</span>

                    </Col>
                    <Col xs={12} md={6}>
                        <div className="p-5">
                            <h2 style={{ fontWeight: 'bold', color: '#FFD23F' }}>
                                Thời gian sử dụng thử của bạn đã hết
                            </h2>
                            <p>Hãy liên hệ với đội ngũ ở Smart German Exam để đăng ký gói và sử dụng dịch vụ của chúng tôi</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>

    );
};

export default ExpiredSubscription;
