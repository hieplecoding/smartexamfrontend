import React, { useState, useEffect } from 'react';
import { Container, Form, Button, Alert, Modal, Card } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import useFetchUserData from './Hooks/useFetchUserData';
import { Image as BootstrapImage } from 'react-bootstrap';
import userProfileImage from '../photos/User.webp';
import '../App.css';
import '../css/UserProfile.css';

const UserProfile = () => {
    const { userData, error } = useFetchUserData();

    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [message, setMessage] = useState('');
    const [showModal, setShowModal] = useState(false);
    const navigate = useNavigate();
    const [passwordValid, setPasswordValid] = useState(false); // State to track password validity
    const [showPassword, setShowPassword] = useState(false);

    // Fetch subscription data


    const handleCloseModal = () => {
        setShowModal(false);
        setMessage('');
    };

    useEffect(() => {
        const strongPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})');
        setPasswordValid(strongPassword.test(newPassword));
    }, [newPassword]);

    //Function for message
    const [alertVariant, setAlertVariant] = useState('info');

    const handleChangePassword = async (e) => {
        e.preventDefault();
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }
        
        if (!passwordValid) {
            setMessage('Mật khẩu phải dài ít nhất 8 ký tự và bao gồm ít nhất một chữ hoa, một chữ thường, một số và một ký tự đặc biệt!');
            setAlertVariant('danger');
            return; // Prevent form submission if the password is not valid
        }

        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/change-password`;
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                old_password: currentPassword,
                new_password: newPassword,
                confirm_new_password: confirmNewPassword,
            })
        })
            .then(async response => {
                const data = await response.json();
                if (!response.ok) {
                    setAlertVariant('danger');
                    setMessage(data.detail || 'Có lỗi xảy ra khi thay đổi mật khẩu.');
                    throw new Error(data.detail || 'Có lỗi xảy ra khi thay đổi mật khẩu.');
                } else {
                    setAlertVariant('primary');
                    setMessage(data.message);
                }
            })
            .catch(error => {
                console.error('Fetch error:', error.message);
                setAlertVariant('danger');
                setMessage(error.message);
            });
    };

    if (error) {
        return <Alert variant="danger">{error}</Alert>;
    }

    const toggleShowPassword = () => setShowPassword(!showPassword);
    return (
        <Container fluid className="user-profile-container">
            <Card className="user-profile-card">
                <Card.Body>
                    <Card.Title style={{ fontWeight: 'bold', fontSize: '35px' }} className="text-center mb-4">Thông tin cá nhân</Card.Title>
                    <div className="profile-header">
                        <div className="user-photo-container">
                            <BootstrapImage src={userProfileImage} roundedCircle className="user-photo" />
                        </div>
                        <div className="user-details">
                            {error && <Alert variant="danger">{error}</Alert>}
                            {userData && (
                                <>
                                    <Form.Group className="mb-3" controlId="formUsername">
                                        <Form.Label>Tên tài khoản</Form.Label>
                                        <Form.Control type="text" value={userData.username} readOnly className="form-input" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email" value={userData.user_email} readOnly className="form-input" />
                                    </Form.Group>
                                </>
                            )}
                        </div>
                    </div>

                    {userData && (
                        <div className="profile-body">
                            <h5 className="section-title">Dịch vụ</h5>
                            <div className="service-info">

                                <Form.Group className="mb-3 service-item">
                                    <Form.Label>Gói</Form.Label>
                                    <div className="form-control-wrapper">
                                        <Form.Control type="text" value={userData.plan_name} readOnly />
                                    </div>
                                </Form.Group>
                                <Form.Group className="mb-3 service-item">
                                    <Form.Label>Hết hạn vào ngày</Form.Label>
                                    <div className="form-control-wrapper">
                                        <Form.Control type="text" value={userData.end_date ? new Date(userData.end_date).toLocaleDateString() : 'N/A'} readOnly />
                                    </div>
                                </Form.Group>
                            </div>
                            <Button variant="primary" onClick={() => setShowModal(true)} className="btn-change-password">
                                Đổi mật khẩu
                            </Button>
                        </div>
                    )}
                    <Modal show={showModal} onHide={handleCloseModal}>
                        <Modal.Header closeButton>
                            <Modal.Title>Đổi mật khẩu</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form onSubmit={handleChangePassword}>
                                {/* Password change form fields */}
                                <Form.Group controlId="formCurrentPassword">
                                    <Form.Control
                                        type={showPassword ? "text" : "password"}
                                        placeholder="Nhập mật khẩu hiện tại"
                                        value={currentPassword}
                                        onChange={(e) => setCurrentPassword(e.target.value)}
                                        className="form-input"
                                    />
                                </Form.Group>
                                <p> </p>
                                <Form.Group controlId="formNewPassword">
                                    <Form.Control
                                        type={showPassword ? "text" : "password"}
                                        placeholder="Nhập mật khẩu mới"
                                        value={newPassword}
                                        onChange={(e) => setNewPassword(e.target.value)}
                                        className="form-input"
                                    />
                                </Form.Group>
                                <p> </p>
                                <Form.Group controlId="formConfirmNewPassword">
                                    <Form.Control
                                        type={showPassword ? "text" : "password"}
                                        placeholder="Xác nhận mật khẩu mới"
                                        value={confirmNewPassword}
                                        onChange={(e) => setConfirmNewPassword(e.target.value)}
                                        className="form-input"
                                    />
                                    <Button style={{ marginTop: '20px', marginBottom: '20px' }} variant="outline-secondary" onClick={toggleShowPassword}>
                                        {showPassword ? 'Ẩn' : 'Hiển thị'}
                                    </Button>
                                </Form.Group>
                                {message && <Alert variant={alertVariant}>{message}</Alert>}
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleCloseModal}>Đóng</Button>
                            <Button variant="primary" onClick={handleChangePassword}>Lưu</Button>
                        </Modal.Footer>
                    </Modal>
                </Card.Body>
            </Card>
        </Container>
    );
};

export default UserProfile;