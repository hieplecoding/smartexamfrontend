// VerifyEmail.js
import React from 'react';
import { useLocation } from 'react-router-dom';
import { Alert, Container, Row, Col, Button } from 'react-bootstrap';

const VerifyEmail = () => {
  const location = useLocation();
  const message = location.state?.message;

  return (
    <Container className="my-5 py-5">
      <Row className="justify-content-center">
        <Col md={8} lg={6}>
          <div className="text-center">
            <h1 className="mb-4">Xác nhận Email</h1>
            <Alert variant="primary" className="shadow-sm">
              {message}
            </Alert>
            <p className="mt-4">
              Hãy làm theo hướng dẫn trong Email để hoàn thành việc đăng ký! Nếu bạn không thấy link xác nhận, làm ơn hãy kiểm tra hộp thư Spam!
            </p>
            <Button href="/" variant="outline-primary" className="mt-3">
              Quay trở lại trang chính
            </Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default VerifyEmail;
