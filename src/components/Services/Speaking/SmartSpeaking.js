import React, { useRef, useState } from 'react';
import useSpeakingThreadId from '../../Hooks/useSpeakingThreadId';
import useDoSpeaking from '../../Hooks/useDoSpeaking';
import { useNavigate } from 'react-router-dom';

const SmartSpeaking = () => {
    const navigate = useNavigate();
    const { threadId, error: threadError, fetchThreadId } = useSpeakingThreadId(navigate);
    const { audioLink, error: audioError, doSpeaking } = useDoSpeaking(navigate);
    const [isRecording, setIsRecording] = useState(false);
    const mediaRecorderRef = useRef(null);
    const audioChunksRef = useRef([]);

    const startRecording = async () => {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            try {
                const audioConstraints = {
                    audio: {
                        echoCancellation: true,
                        noiseSuppression: true,
                        autoGainControl: true
                    }
                };
                const stream = await navigator.mediaDevices.getUserMedia(audioConstraints);
                
                let options = {};
                if (MediaRecorder.isTypeSupported('audio/webm')) {
                    options.mimeType = 'audio/webm';
                } else {
                    console.log('Using the default MIME type as preferred type is not supported.');
                }

                mediaRecorderRef.current = new MediaRecorder(stream, options);
                audioChunksRef.current = [];

                mediaRecorderRef.current.ondataavailable = (event) => {
                    if (event.data.size > 0) {
                        audioChunksRef.current.push(event.data);
                    }
                };

                mediaRecorderRef.current.onstop = async () => {
                    const audioBlob = new Blob(audioChunksRef.current, { type: mediaRecorderRef.current.mimeType });
                    console.log('Recording stopped, MIME type:', audioBlob.type);
                    await doSpeaking(audioBlob);
                };

                mediaRecorderRef.current.start();
                setIsRecording(true);
            } catch (error) {
                console.error('Error accessing microphone:', error);
            }
        } else {
            console.error('Microphone access is not supported by your browser.');
        }
    };

    const stopRecording = () => {
        if (mediaRecorderRef.current && mediaRecorderRef.current.state !== 'inactive') {
            mediaRecorderRef.current.stop();
            setIsRecording(false);
        }
    };

    return (
        <div>
            <h1>Smart Speaking Test</h1>
            {threadId && <p>Thread ID: {threadId}</p>}
            {threadError && <p>Error: {threadError}</p>}
            {audioLink && (
                <div>
                    <p>Audio Link: <a href={audioLink} target="_blank" rel="noopener noreferrer">Play Audio</a></p>
                    <audio src={audioLink} controls />
                </div>
            )}
            {audioError && <p>Error: {audioError}</p>}
            <button onClick={fetchThreadId}>Create/Fetch Thread ID</button>
            {isRecording ? (
                <button onClick={stopRecording}>Stop Recording</button>
            ) : (
                <button onClick={startRecording}>Start Recording</button>
            )}
            <button onClick={() => setIsRecording(false)}>Reset</button>
        </div>
    );
};

export default SmartSpeaking;
