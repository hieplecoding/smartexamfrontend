import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import useFetchWritingQuestions from './Hooks/useFetchWritingQuestion';
import '../App.css';
import image from '../photos/Picture 3.webp';
import '../css/IntroductionSectionInAll.css';
import '../css/SmartEssay.css';

const SmartCheckingEssay = () => {
  const location = useLocation();
  const { service, level } = location.state || {};
  const [content, setContent] = useState('');
  const [serverResponse, setServerResponse] = useState('');
  const navigate = useNavigate();
  const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/checking_essay`;
  const questions = useFetchWritingQuestions(service, level, navigate);

  const [selectedQuestionId, setSelectedQuestionId] = useState('');
  const [selectedQuestionText, setSelectedQuestionText] = useState(''); // New state for selected question text
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      navigate('/login');
    }
  }, [navigate]);

  const handleSubmit = (event, typeOfCheck) => {
    event.preventDefault();
    setIsLoading(true);
    const token = localStorage.getItem('token');
    if (!token) {
      navigate('/login');
      return;
    }

    // Constructing the URL with a query parameter for the type of check
    const urlWithQuery = `${apiUrl}?way_to_check=${typeOfCheck}`;

    fetch(urlWithQuery, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        question_id: selectedQuestionId,
        content
      })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Request failed');
        }
        return response.json();
      })
      .then(data => {
        setIsLoading(false);
        if (data && data.response && data.response.content) {
          setServerResponse(data.response.content);
        } else {
          throw new Error('Invalid response structure');
        }
      })
      .catch(error => {
        setIsLoading(false);
        console.error('Error:', error);
      });
  };


  // Update both selectedQuestionId and selectedQuestionText on change
  const handleSelectChange = (e) => {
    const selectedId = e.target.value;
    setSelectedQuestionId(selectedId);

    const selectedText = e.target.options[e.target.selectedIndex].text;
    setSelectedQuestionText(selectedText === "Câu hỏi..." ? "" : selectedText); // Avoid setting the placeholder as the question text
  };

  return (
    <>
      <div className="intro-section">
        <div className="container ">
          <div className="row align-items-center">
            {/* Apply Bootstrap's 'col' classes for grid layout and your custom classes for styling and animations */}
            <div className="col-12 col-md-6 intro-text-content">
              <h2>Smart Essay</h2>
              <p>Tại đây các bạn sẽ được luyện tập khả năng viết thông qua sự hỗ trợ của AI trong việc sửa ngữ pháp cũng như đề xuất các ý tưởng.</p>
            </div>
            <div className="col-12 col-md-6 intro-image-content">
              <img src={image} alt="Introduction" className="intro-image img-fluid" />
            </div>
          </div>
        </div>
      </div>



      <div className="scrollable-container">
        <div className="container my-5">
          {questions && questions.length > 0 && (
            <div className="mb-3">
              <label htmlFor="questionSelect" className="form-label">Chọn câu hỏi:</label>
              <select
                id="questionSelect"
                className="form-control"
                value={selectedQuestionId}
                onChange={handleSelectChange}
              >
                <option value="">Câu hỏi...</option>
                {questions.map((question) => (
                  <option key={question.question_id} value={question.question_id}>
                    {question.information_text}
                  </option>
                ))}
              </select>
              {/* Directly display the selected question text */}
              {selectedQuestionText && (
                <div className="mt-3">
                  <strong>Câu hỏi:</strong> {selectedQuestionText}
                </div>
              )}
            </div>
          )}


          <form onSubmit={handleSubmit} className="mb-3">
            <div className="form-group">
              <textarea
                className="form-control"
                placeholder="Viết bài viết của bạn ở đây"
                value={content}
                onChange={(e) => setContent(e.target.value)}
                rows="10"
                style={{ resize: 'vertical' }}
              />
            </div>
            <div className="text-center" style={{ marginTop: '35px' }}>
              <div className="row justify-content-center">
                <div className="col-auto">
                  <button type="button" className="btn btn-primary mt-2 me-2" onClick={(e) => handleSubmit(e, 'grammar_check')}>
                    Kiểm tra ngữ pháp
                  </button>
                </div>
                <div className="col-auto">
                  <button type="button" className="btn btn-primary mt-2" onClick={(e) => handleSubmit(e, 'essay_check')}>
                    Kiểm tra bài viết
                  </button>
                </div>
              </div>
            </div>

          </form>
          {isLoading ? (
            <div className="load-robot-container">
              <span className="material-symbols-outlined load-robot">smart_toy</span>
              <span className="loading-text">Robot đang cố gắng sửa bài viết cho bạn, hãy đợi 1 xíu nha...</span>
            </div>
          ) : serverResponse ? (
            <div className={`alert ${serverResponse === "Es ist richtig" ? "alert-success" : "alert-warning"}`}>
              Nhận xét: {serverResponse}
            </div>
          ) : null}
        </div>
      </div>
    </>
  );
};

export default SmartCheckingEssay;
