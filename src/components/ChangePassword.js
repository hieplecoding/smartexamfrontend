import React, { useState, useEffect } from 'react';
import { Form, Button, Alert, Container, InputGroup, Card } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';

const ChangePassword = () => {
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const [message, setMessage] = useState('');
    const [alertVariant, setAlertVariant] = useState('info');
    const [countdown, setCountdown] = useState(null); // New state for countdown

    const navigate = useNavigate();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const token = queryParams.get('token');

    useEffect(() => {
        if (countdown === 0) {
            navigate('/login'); // Redirect to login when countdown reaches 0
        } else if (countdown > 0) {
            const timerId = setTimeout(() => setCountdown(countdown - 1), 1000);
            return () => clearTimeout(timerId);
        }
    }, [countdown, navigate]);

    const handleChangePassword = async (e) => {
        e.preventDefault();

        if (newPassword !== confirmPassword) {
            setMessage("Mật khẩu mới và xác nhận mật khẩu không khớp.");
            setAlertVariant('danger');
            return;
        }

        fetch(`${process.env.REACT_APP_API_BASE_URL}/reset-password`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token, 
                new_password: newPassword,
            }),
        })
            .then(async response => {
                const data = await response.json();
                if (!response.ok) {
                    setAlertVariant('danger');
                    setMessage(data.detail || 'Có lỗi xảy ra khi thay đổi mật khẩu.');
                    throw new Error(data.detail || 'Failed to change password.');
                } else {
                    setAlertVariant('primary');
                    setMessage('Mật khẩu của bạn đã được thay đổi thành công. Vui lòng đăng nhập lại!');
                    setCountdown(5);
                }
            })
            .catch(error => {
                console.error('Fetch error:', error.message);
                setMessage(error.message);
                setAlertVariant('danger');
            });
    };

    const toggleShowPassword = () => setShowPassword(!showPassword);

    return (
        <Container className="d-flex align-items-center justify-content-center" style={{ minHeight: "80vh" }}>
            <Card className="w-100" style={{ maxWidth: "400px" }}>
                <Card.Body>
                    <h2 className="text-center mb-4">Thay Đổi Mật Khẩu</h2>
                    <Form onSubmit={handleChangePassword}>
                        <Form.Group className="mb-3" controlId="formNewPassword">
                            <InputGroup>
                                <Form.Control
                                    type={showPassword ? "text" : "password"}
                                    placeholder="Mật khẩu mới"
                                    value={newPassword}
                                    onChange={(e) => setNewPassword(e.target.value)}
                                />
                                <Button variant="outline-secondary" onClick={toggleShowPassword}>
                                    {showPassword ? 'Ẩn' : 'Hiển thị'}
                                </Button>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formConfirmPassword">
                            <InputGroup>
                                <Form.Control
                                    type={showPassword ? "text" : "password"}
                                    placeholder="Xác nhận mật khẩu mới"
                                    value={confirmPassword}
                                    onChange={(e) => setConfirmPassword(e.target.value)}
                                />
                                <Button variant="outline-secondary" onClick={toggleShowPassword}>
                                    {showPassword ? 'Ẩn' : 'Hiển thị'}
                                </Button>
                            </InputGroup>
                        </Form.Group>

                        <Button variant="primary" type="submit" className="w-100">
                            Thay Đổi
                        </Button>
                    </Form>

                    {message && (
                        <Alert variant={alertVariant} className="mt-3">
                            {message}
                        </Alert>
                    )}

                    {/* Countdown display */}
                    {countdown !== null && (
                        <div className="text-center mt-3" style={{ fontSize: '24px' }}>
                            Quay lại trang đăng nhập trong {countdown}...
                        </div>
                    )}
                </Card.Body>
            </Card>
        </Container>
    );
};

export default ChangePassword;
