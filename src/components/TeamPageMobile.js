import React, { useEffect } from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import '../css/TeamPageMobile.css';
import image1 from '../photos/dan.webp';
import image2 from '../photos/duong.webp';
import image3 from '../photos/hiep.webp';

const TeamPageMobile = () => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <Container className="team-container-mobile">
            {/* Member 1 */}
            <Row className="team-row-mobile team-member-1">
                <Col xs={12} className="d-flex flex-column align-items-center justify-content-center">
                    <div className="image-background-circle-mobile">
                        <Image src={image3} className="team-image-mobile" />
                    </div>
                    <div className="team-info-mobile">
                        <h3>Hiệp Lê</h3>
                        <p className="role-mobile">Founder/Developer</p>
                        <p>Hiện Hiệp đang là kĩ sư phần mềm tại công ty Austrian Airlines và sinh viên thuộc ngành khoa học máy tính trường đại học ứng dụng kĩ thuật Technikum Wien ở thủ độ Vienna của Áo. Cùng với niềm đam mê về công nghệ và AI, Hiệp còn dành nhiều thời gian nghiên cứu về việc học tiếng Đức. Hiệp Lê đã đạt được nhiều bằng tiếng Đức như B1 Goethe, B2-C1 VWU Wien.</p>
                    </div>
                </Col>
            </Row>

            {/* Member 2 */}
            <Row className="team-row-mobile team-member-2">
                <Col xs={12} className="d-flex flex-column align-items-center justify-content-center">
                    <div className="image-background-circle-mobile">
                        <Image src={image2} className="team-image-mobile" />
                    </div>
                    <div className="team-info-mobile">
                        <h3>Nguyễn Bình Dương</h3>
                        <p className="role-mobile">Developer</p>
                        <p>Dương hiện là sinh viên ngành công nghệ thông tin theo học trường đại học ứng dụng kĩ thuật Technikum Wien tại thủ đô Viên xinh đẹp của nước Áo đồng thời đang làm việc cho một công ty phần mềm. Với niềm đam mê lớn dành cho lập trình, Dương đóng vai trò quan trọng trong phát triển, thiết kế nên trang web này.</p>
                    </div>
                </Col>
            </Row>

            {/* Member 3 */}
            <Row className="team-row-mobile team-member-3">
                <Col xs={12} className="d-flex flex-column align-items-center justify-content-center">
                    <div className="image-background-circle-mobile">
                        <Image src={image1} className="team-image-mobile" />
                    </div>
                    <div className="team-info-mobile">
                        <h3>Phạm Tâm Đan</h3>
                        <p className="role-mobile">Content Creator</p>
                        <p>Tâm Đan hiện đang là sinh viên ngành Sinh học của đại học Universität Wien tại Áo. Đan đã có những khoảng thời gian sinh sống, lớn lên tại Đức từ nhỏ và hiện đang học tập tại Áo, cũng là một đất nước nói tiếng Đức. Bên cạnh đó Đan cũng là dịch giả của cuốn sách cho thiếu nhi "Gấu gặm" của Annette Pehnt. Chính vì vậy, Tâm Đan là người chịu trách nhiệm chính cho việc soạn nội dung, bộ câu hỏi cho trang web này.</p>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default TeamPageMobile;
