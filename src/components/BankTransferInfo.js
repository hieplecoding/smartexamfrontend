import React from 'react';
import '../css/BankTransferInfo.css'; // Ensure the path to your CSS file is correct
import { Container } from 'react-bootstrap';
import vietcombankLogo from '../photos/bank/vietcombank.png'; // Adjust the path as needed
import techcombankLogo from '../photos/bank/techcombank.png'; // Adjust the path as needed

const BankTransferInfo = () => {
    return (
        <Container className='custom-container'>
            <div className="bank-transfer-container">
                <h2>Thông tin chuyển khoản</h2>
                <p style={{ textAlign: 'left' }}>
                    Gửi thông tin chuyển tiền và tên tài khoản cho email: smartgermanexam@gmail.com hoặc page sau khi chuyển khoản
                </p>

                <div className="bank-details">
                    <div className="bank">
                        <img src={vietcombankLogo} alt="Vietcombank" />
                        <div className="bank-info">
                            <p><span className="bold-text">STK:</span> 9353936037</p>
                            <p><span className="bold-text">Chủ tài khoản:</span> Lê Duy Hiệp</p>
                            <p><span className="bold-text">Chi Nhánh:</span> Vietcombank (VCB) - Chi nhánh TP. Hồ Chí Minh</p>
                        </div>
                    </div>
                    <div className="bank">
                        <img src={techcombankLogo} alt="Techcombank" />
                        <div className="bank-info">
                            <p><span className="bold-text">STK:</span> 1999231007</p>
                            <p><span className="bold-text">Chủ tài khoản:</span> Lê Duy Hiệp</p>
                            <p><span className="bold-text">Chi Nhánh:</span> Techcombank (TCB) - Chi nhánh TP. Hồ Chí Minh</p>
                        </div>
                    </div>

                    {/* Repeat for other banks */}
                </div>
            </div>
        </Container>
    );
};

export default BankTransferInfo;
