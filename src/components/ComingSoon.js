import React from 'react';
import { Row, Col , Container} from 'react-bootstrap';
import '../css/ComingSoon.css'
const ComingSoon = () => {


    return (
        <>
            <Container>
                <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, backgroundColor: '#FFFFFF', borderRadius: '25px', padding: '0' }}>
                    <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>settings</span>

                    </Col>
                    <Col xs={12} md={6}>
                        <div className="p-5">
                            <h2 style={{ fontWeight: 'bold', color: '#FFD23F' }}>
                                Đang phát triển
                            </h2>
                            <p>Smart German Exam sẽ cố gắng hoàn thiện và đưa đến bạn tính năng này với chất lượng tốt nhất!</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>

    );
};

export default ComingSoon;
