import { Container, Row, Col, Card, Button, Carousel } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import image from '../photos/writing.webp';
import image1 from '../photos/listening.webp';
import image2 from '../photos/grammar.webp';
import Pic1 from '../photos/pic1-landingpage2.webp';
import Pic2 from '../photos/pic2-landingpage2.webp';
import Pic3 from '../photos/pic3-landingpage2.webp';
import { useNavigate, useLocation } from 'react-router-dom';
import '../css/LandingPage2.css'
import IntroductionLanding2 from './IntroductionLanding2';

const imageStyle = {
  width: '100%',
  height: '100%',
  maxWidth: '70%',
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
  boxShadow: '0 4px 12px rgba(0, 0, 0, 0.15)',
  borderRadius: '10px',
  marginBottom: '10px',
};

const circleImageStyle = {
  ...imageStyle,
  width: '100%',
  borderRadius: '50%',
  marginBottom: '20px'
};

const carouselItems = [
  {
    src: image,
    alt: "Smart German",
    header: "Smart German",
    caption: "Bài viết thông minh với khả năng sửa lỗi cho bạn",
  },
  {
    src: image1,
    alt: "Smart Listening",
    header: "Smart Listening",
    caption: "Luyện nghe qua audio thông minh theo từng chủ đề",
  },
  {
    src: image2,
    alt: "Smart Grammar",
    header: "Smart Grammar",
    caption: "Bộ câu hỏi ngữ pháp thông minh theo từng chủ đề",
  }
];

const IntroductionContent = () => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <div>
      <IntroductionLanding2></IntroductionLanding2>
      <Carousel activeIndex={index} onSelect={handleSelect} interval={3000} controls={true} fade={true}>
        {carouselItems.map((item, idx) => (
          <Carousel.Item key={idx}>
            <LazyLoadImage
              src={item.src}
              alt={item.alt}
              style={imageStyle}
            />
          </Carousel.Item>
        ))}
      </Carousel>
      <div style={{ textAlign: 'center', marginTop: '20px' }}>
        <h3>{carouselItems[index].header}</h3>
        <p>{carouselItems[index].caption}</p>
      </div>
    </div>
  );
};

const LandingPage2 = () => {
  const navigate = useNavigate();
  const [showLevels, setShowLevels] = useState(null);
  const location = useLocation(); 

  useEffect(() => {
    if (location.state?.scrollTo === 'skills') {
      setTimeout(() => {
        window.scrollTo({
          left: 0,
          top: document.body.scrollHeight,
          behavior: 'smooth',
        });
      }, 0);
    }
  }, [location.state]); 

  const services = [
    { src: Pic1, title: 'Smart Essay', description: 'Luyện kĩ năng viết.', levels: ['A1', 'A2', 'B1', 'B2'], path: '/smart-checking-essay' },
    { src: Pic2, title: 'Smart Listening', description: 'Luyện kĩ năng nghe.', levels: ['A1', 'A2', 'B1', 'B2'], path: '/smart-listening-question' },
    { src: Pic3, title: 'Smart Grammar', description: 'Luyện ngữ pháp.', path: '/grammar-question' },
  ];

  const handleServiceSelection = (index, service) => {
    if (service.levels) {
      setShowLevels(showLevels === index ? null : index);
    } else {
      navigate(service.path);
    }
  };

  const handleLevelSelection = (service, level) => {
    if (['B1', 'B2'].includes(level)) {
      navigate('/coming-soon'); 
    } else {
      navigate(service.path, { state: { level } });
      window.scrollTo(0, 0);
    }
  };  

  const getButtonClass = (level) => {
    const baseClass = "m-1 level-button button-";
    switch(level) {
      case 'A1': return baseClass + 'A1';
      case 'A2': return baseClass + 'A2';
      case 'B1': return baseClass + 'B1';
      case 'B2': return baseClass + 'B2';
      default: return baseClass;
    }
  };

  return (
    <Container className="custom-container-2 mt-5">
      <div className="col-12 text-center mb-3">
        <IntroductionContent />
      </div>
      <hr color="blue" width="55%" style={{ marginBottom: '65px' }} />
      <Row xs={1} md={2} lg={3} className="g-4" style={{ marginBottom: '40px' }}>
        {services.map((service, index) => (
          <Col key={service.title}>
            <Card style={{ border: 'none' }}>
              <div onClick={() => handleServiceSelection(index, service)}>
                <LazyLoadImage
                  src={service.src}
                  alt={service.title}                 
                  style={circleImageStyle}
                  className="image-hover-effect"
                />
                {showLevels === index ? (
                  <div className="level-buttons-container">
                    {service.levels.map(level => (
                      <Button key={level} variant="secondary" className={getButtonClass(level)} onClick={(e) => {
                        e.stopPropagation();                       
                        handleLevelSelection(service, level);
                      }}>{level}</Button>
                    ))}
                  </div>
                ) : (
                  <>
                    <Button
                      className="card-title-button mt-2"
                      variant="secondary"
                      onClick={() => handleServiceSelection(index, service)}
                    >
                      {service.title}
                    </Button>
                    <Card.Text className="mt-2">{service.description}</Card.Text>
                  </>
                )}
              </div>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default LandingPage2;