import React from 'react';


import { Container, Row, Col, Button } from 'react-bootstrap';

const HandleVerify = () => {
  
  
  

  return (
    <Container className="my-5 py-5">
      <Row className="justify-content-center">
        <Col md={8} lg={6}>
          <div className="text-center">
            <h1 className="mb-4">Email của bạn đã được xác nhận</h1>
            
            <p className="mt-4">
              Nếu xảy ra bất kì lỗi nào trong quá trình đăng nhập. Hãy liên hệ đội ngũ Smart German Exam.
            </p>
            <Button href="/login" variant="outline-success" className="mt-3">
              Go to Login Page
            </Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default HandleVerify;
