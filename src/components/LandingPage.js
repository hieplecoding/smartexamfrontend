import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import useOnScreen from './Hooks/useOnScreen';
import IntroductionSection from './IntroductionSection';
import IntroduceFeature from './IntroduceFeature';
import '../css/AnimationForLandingPage.css'
import Subscription from './Subscription';

const LandingPage = () => {
  // Refs for each row
  const refFirst = useRef();
  const refSecond = useRef();
  const refThird = useRef();

  // Visibility states
  const isVisibleFirst = useOnScreen(refFirst);
  const isVisibleSecond = useOnScreen(refSecond);
  const isVisibleThird = useOnScreen(refThird);
  // States to track if animation has been applied
  const [animatedFirst, setAnimatedFirst] = useState(false);
  const [animatedSecond, setAnimatedSecond] = useState(false);
  const [animatedThird, setAnimatedThird] = useState(false);
  // Check and set animation states based on visibility
  useEffect(() => {
    if (isVisibleFirst && !animatedFirst) {
      setAnimatedFirst(true); // Ensures animation runs once
    }
  }, [isVisibleFirst, animatedFirst]);

  useEffect(() => {
    if (isVisibleSecond && !animatedSecond) {
      setAnimatedSecond(true); // Ensures animation runs once
    }
  }, [isVisibleSecond, animatedSecond]);

  useEffect(() => {
    if (isVisibleThird && !animatedThird) {
      setAnimatedThird(true); // Ensures animation runs once
    }
  }, [isVisibleThird, animatedThird]);

  const navigate = useNavigate();
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      navigate('/'); // Redirect to login if no token is found
    } else {
      navigate('/landing-2');
    }
  }, [navigate]);
  return (
    <>
      <IntroductionSection />

      <Container className="custom-container">
        <div style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
          <Row className="w-100 my-4 custom-border" style={{
            marginRight: 0,
            marginLeft: 0,
            backgroundColor: '#FFFFFF',
            borderRadius: '25px',
            padding: '0'
          }}>
            <Col>
              <div className="p-5">
                <h2 style={{
                  fontWeight: 'bold' // This will apply bold styling to the h2 element
                }}>
                  Khám phá Ngôn ngữ Đức
                </h2>
                <p>
                  Tham gia cùng chúng tôi và bắt đầu học tiếng Đức một cách vui vẻ và dễ dàng! Với sự giúp đỡ của
                  AI thân thiện của chúng tôi, bạn sẽ nhận được các mẹo và thủ thuật được cá nhân hóa chỉ dành cho bạn.
                  Cho dù đó là từ vựng, câu, hay đoạn hội thoại, bạn sẽ ngày càng tiến bộ hơn mỗi ngày.
                  Bắt đầu hành trình của bạn để nói tiếng Đức như một chuyên gia, với rất nhiều sự hỗ trợ và nụ cười trên
                  con đường phía trước.
                </p>
                <Link to="/login">
                  <Button variant="btn btn-success" size="lg">
                    Bắt đầu
                  </Button>
                </Link>
              </div>
            </Col>
          </Row>
        </div>

        <div ref={refFirst} className={(isVisibleFirst && animatedFirst) ? 'slide-in-left' : ''} style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
          <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, backgroundColor: '#FFFFFF', borderRadius: '25px', padding: '0' }}>
            <Col xs={12} md={6}>
              <div className="p-5">
                <h2 style={{ fontWeight: 'bold', color: '#FFD23F' }}>
                  Nguồn bài tập tiếng Đức phong phú và chất lượng
                </h2>
                <p>Tài liệu và bài tập chuẩn do đội ngũ Smart German Exam biên soạn</p>
              </div>
            </Col>
            <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>import_contacts</span>
            </Col>
          </Row>
        </div>

        <div ref={refSecond} className={(isVisibleSecond && animatedSecond) ? 'slide-in-right' : ''} style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
          <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, backgroundColor: '#FFFFFF', borderRadius: '25px', padding: '0' }}>
            <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>robot_2</span>
            </Col>
            <Col xs={12} md={6}>
              <div className="p-5">
                <h2 style={{ fontWeight: 'bold', color: '#FFD23F' }}>
                  Smart Exam với AI train cho riêng việc học tiếng đức
                </h2>
                <p>Kiểm tra và check kết quả bằng AI, đem lại kết quả nhanh và chính xác</p>
              </div>
            </Col>
          </Row>
        </div>

        <div ref={refThird} className={(isVisibleThird && animatedThird) ? 'slide-in-left' : ''} style={{ marginTop: '50px', marginBottom: '50px', textAlign: 'center' }}>
          <Row className="w-100 my-4" style={{ marginRight: 0, marginLeft: 0, backgroundColor: '#FFFFFF', borderRadius: '25px', padding: '0' }}>
            <Col xs={12} md={6}>
              <div className="p-5">
                <h2 style={{ fontWeight: 'bold', color: '#FFD23F' }}>
                  Đồng hành cùng bạn làm việc học trở nên hấp dẫn
                </h2>
                <p>Cộng đồng học và ôn thi tiếng Đức</p>
              </div>
            </Col>
            <Col xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <span className="material-symbols-outlined" style={{ fontSize: '200px' }}>sentiment_very_satisfied</span>
            </Col>
          </Row>
        </div>
        
      </Container>

      <IntroduceFeature />
      <Subscription />

    </>
  );
};

export default LandingPage;
