import React, { useState } from 'react';
import PopupAkkusativ from './popups/PopupAkkusativ';
import PopupDativ from './popups/PopupDativ';
import PopupBestimmteArtikel from './popups/PopupBestimmteAktikel';
import PopupAdjektiveBestimmteArtikel from './popups/PopupAdjektivBestimmteArtikel';
import PopupUnbestimmteArtikel from './popups/PopupUnbestimmteArtikel';
import PopupAdjektiveUnbestimmteArtikel from './popups/PopupAdjektivUnbestimmteArtikel';
import PopupNominativ from './popups/PopupNominativ';
import PopupGenitiv from './popups/PopupGenitiv';
import RelativSatz from './popups/RelativSatz';

const GrammarQuestion = ({ question, onCorrectAnswer, isPopupActive, onPopupToggle, isLocked }) => {
    const [userAnswer, setUserAnswer] = useState('');
    const [serverResponse, setServerResponse] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        if (isLocked) return;
        setIsLoading(true);
        const token = localStorage.getItem('token');
        if (!token) {
            // Handle the case where the user is not logged in
            return;
        }
        //process.env.REACT_APP_API_BASE_URL
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/grammar_questions`;
        const requestBody = {
            id_question: question.id_question,
            user_answer: userAnswer,
            question_type: question.question_type
        };
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(requestBody)
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                setIsLoading(false);

                if (typeof data.response === 'string' && data.response === "Es ist richtig") {
                    setServerResponse(data.response);
                    onCorrectAnswer();
                }
                else if (typeof data.response === 'string' && data.response === "Es ist falsch") {
                    setServerResponse(data.response);
                }

                else if (data.response && data.response.content) {
                    setServerResponse(data.response.content);
                } else {
                    console.error('Unexpected response structure:', data);
                    setServerResponse('Unexpected response structure. Please contact support.');
                }
            })
            .catch(error => {
                setIsLoading(false);
                console.error(`Error submitting question ${question.id_question}:`, error.message);
                setServerResponse(`Error: ${error.message}`);
            });
    };

    const handleOptionChange = (changeEvent) => {
        setUserAnswer(changeEvent.target.value);
    };
    const togglePopup = () => {
        onPopupToggle();
    };

    const informationTextParts = question.information_text.split('...');

    return (
        <>
            <div className="container mt-1">
                <div className="card shadow-sm p-3 bg-white rounded" style={{ borderRadius: '25px' }}>
                    <div className="card-body">
                        <form onSubmit={handleSubmit}>
                            {question.question_type === "multiple_choice" ? (
                                <>
                                    <h5 className="card-title">{question.information_text}</h5>
                                    <h5 className="card-title">{question.question}</h5>
                                    {['answer_1', 'answer_2', 'answer_3'].map((answerKey) => (
                                        <div key={answerKey} className="mb-3 form-check">
                                            <input
                                                className="form-check-input"
                                                type="radio"
                                                name="answer"
                                                id={answerKey}
                                                value={answerKey}
                                                checked={userAnswer === answerKey}
                                                onChange={handleOptionChange}
                                                style={{ borderRadius: '25px' }}
                                                disabled={isLocked}
                                            />
                                            <label className="form-check-label" htmlFor={answerKey}>
                                                {question[answerKey]}
                                            </label>
                                        </div>
                                    ))}
                                </>
                            ) : (
                                <>
                                    <div className="sentence-container" style={{
                                        display: 'flex',
                                        flexDirection: 'row', // Inline elements horizontally
                                        flexWrap: 'wrap', // Allow wrapping for smaller screens or longer text
                                        alignItems: 'center', // Align elements vertically to the center
                                        justifyContent: 'start', // Align content to the start to keep the left side neat
                                        margin: '0', // No outer margin to mess up alignment with card boundaries
                                        width: '100%', // Full width to utilize the space within the card
                                        padding: '10px 0' // Padding at top and bottom for visual spacing, none on the sides
                                    }}>
                                        <h5 className="card-title" style={{
                                            margin: '0 10px 0 0', // Right margin for spacing, no left margin
                                            whiteSpace: 'nowrap', // Keep this element on one line
                                            flex: 'none' // Do not allow the element to grow or shrink
                                        }}>
                                            {informationTextParts[0]}
                                        </h5>
                                        <input
                                            type="text"
                                            className="form-control"
                                            value={userAnswer}
                                            onChange={handleOptionChange}
                                            disabled={isLocked}
                                            style={{
                                                flex: 'none', // Avoid flexing to keep size consistent
                                                width: '100px', // Fixed width
                                                margin: '0 5px', // Reduced right margin to decrease space
                                                lineHeight: '1.5', // Line height to match surrounding text
                                                textAlign: 'center' // Center text inside the input
                                            }}
                                        />
                                        <h5 className="card-title" style={{
                                            display: 'flex',
                                            flexDirection: 'col', // Inline elements horizontally
                                            flexWrap: 'wrap', // Allow wrapping for smaller screens or longer text
                                            alignItems: 'justifyr', // Align elements vertically to the center
                                            justifyContent: 'start', // Align content to the start to keep the left side neat
                                            margin: '0', // No outer margin to mess up alignment with card boundaries
                                            width: '100%', // Full width to utilize the space within the card
                                            padding: '10px 0' // Padding at top and bottom for visual spacing, none on the sides
                                        }}>
                                            {informationTextParts[1]}
                                        </h5>
                                    </div>
                                    <h5 style={{ marginTop: '20px' }} className="card-title">{question.question}</h5>
                                </>
                            )}
                            <div className="col-md-6"> {/* Grid column for the button */}
                                <button type="submit" className="btn btn-primary mt-2 w-100">Trả lời</button>
                            </div>
                        </form>
                        <div className="col-md-6"> {/* Grid column for the link */}
                            <a href="#!" onClick={togglePopup} style={{
                                background: 'none',
                                border: 'none',
                                color: '#5B7C99', // Adjusted to match your color preference
                                textDecoration: 'none',
                                display: 'block',
                                marginTop: '10px',
                                cursor: 'pointer',
                                padding: 0,
                                fontSize: '16px', // Example font size, adjust as needed
                                fontWeight: 'bold'
                            }}>Trợ giúp ngữ pháp thông minh</a>
                        </div>
                    </div>
                </div>
                {/* Conditional rendering based on question.topic and showPopup state */}
                {isPopupActive ? (
                    question.topic === 'dativ' ? <PopupDativ isOpen={true} onClose={togglePopup} />
                        : question.topic === 'akkusativ' ? <PopupAkkusativ isOpen={true} onClose={togglePopup} />
                            : question.topic === 'bestimmterArtikel' ? <PopupBestimmteArtikel isOpen={true} onClose={togglePopup} />
                                : question.topic === 'akjektiveBestimmterArtikel' ? <PopupAdjektiveBestimmteArtikel isOpen={true} onClose={togglePopup} />
                                    : question.topic === 'unbestimmterArtikel' ? <PopupUnbestimmteArtikel isOpen={true} onClose={togglePopup} />
                                        : question.topic === 'adjektiveUnbestimmteArtikel' ? <PopupAdjektiveUnbestimmteArtikel isOpen={true} onClose={togglePopup} />
                                            : question.topic === 'nominativ' ? <PopupNominativ isOpen={true} onClose={togglePopup} />
                                                : question.topic === 'genitiv' ? <PopupGenitiv isOpen={true} onClose={togglePopup} />
                                                    : question.topic === 'relativSatz' ? <RelativSatz isOpen={true} onClose={togglePopup} />
                                                        : null
                ) : null}
                {isLoading ? (
                    <div className="loader"></div> // Show loading icon when isLoading is true
                ) : serverResponse ? (
                    <div className={`alert ${serverResponse === "Es ist richtig" ? "alert-success" : "alert-warning"}`}>
                        Response: {serverResponse}
                    </div>
                ) : null}
            </div>
        </>
    );
};

export default GrammarQuestion;
