import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import { Navbar as BootstrapNavbar, Nav, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useAuth } from '../Hooks/useAuth';
import { useNavigate } from 'react-router-dom';
import logo from '../../photos/logo.webp';
import '../../App.css';
import '../../css/ForLogo.css';
import '../../css/Navbar.css';

function ColorSchemesExample() {
    const [isSticky, setIsSticky] = useState(true);  // State to control the sticky behavior
    const auth = useAuth();
    const navigate = useNavigate();

    // Function to navigate with state
    const navigateTo = (path, state = {}) => {
        navigate(path, { state });
    };

    // Function to toggle navbar stickiness
    const toggleNavbarStickiness = (shouldStick) => {
        setIsSticky(shouldStick);
    };

    // Expose the function globally to allow popups to call it
    window.toggleNavbarStickiness = toggleNavbarStickiness;

    return (
        <BootstrapNavbar collapseOnSelect expand="lg" bg="" variant=""
            className={`my-navbar py-0 ${isSticky ? 'sticky-top' : ''}`}>
            <Container fluid className="py-0">
                {auth.isAuthenticated ? (
                    <LinkContainer to="/landing-2">
                        <BootstrapNavbar.Brand>
                            <img src={logo} alt="Smart German Exam Logo" className="logo-hover-effect" style={{ width: '88px', height: '90px' }} />
                            <span className="website-name">Smart German Exam</span>
                        </BootstrapNavbar.Brand>
                    </LinkContainer>
                ) : (
                    <LinkContainer to="/">
                        <BootstrapNavbar.Brand>
                            <img src={logo} alt="Smart German Exam Logo" className="logo-hover-effect" style={{ width: '88px', height: '90px' }} />
                            <span className="website-name">Smart German Exam</span>
                        </BootstrapNavbar.Brand>
                    </LinkContainer>
                )}

                <BootstrapNavbar.Toggle aria-controls="basic-navbar-nav" />
                <BootstrapNavbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        {!auth.isAuthenticated && (
                            <>
                                <LinkContainer to="/register">
                                    <Nav.Link>Đăng ký</Nav.Link>
                                </LinkContainer>
                                <div className="login-highlight">
                                    <LinkContainer to="/login">
                                        <Nav.Link>Đăng nhập</Nav.Link>
                                    </LinkContainer>
                                </div>
                            </>
                        )}
                        {auth.isAuthenticated && (
                            <>
                                <NavDropdown title="Tiện ích" id="navbarScrollingDropdown" className="nav-dropdown-custom">
                                    <NavDropdown.Item as="button" onClick={() => navigateTo('/landing-2')} className="dropdown-item">
                                        Giới thiệu
                                    </NavDropdown.Item>
                                    <NavDropdown.Item as="button" onClick={() => navigateTo('/landing-2', { scrollTo: 'skills' })} className="dropdown-item">
                                        Các kỹ năng
                                    </NavDropdown.Item>
                                    <NavDropdown.Item as="button" onClick={() => navigateTo('/team-sge')} className="dropdown-item">
                                        Về team SGE
                                    </NavDropdown.Item>
                                    <NavDropdown.Item as="button" onClick={() => navigateTo('/contact')} className="dropdown-item">
                                        Liên hệ
                                    </NavDropdown.Item>
                                </NavDropdown>

                                <LinkContainer to="/user-profile" className="gooey-effect">
                                    <Nav.Link title="User Profile">
                                        <span className="material-icons" style={{ fontSize: '1.5rem' }}>account_circle</span>
                                    </Nav.Link>
                                </LinkContainer>

                                <Nav.Link onClick={auth.logout} title="Logout" className="gooey-effect">
                                    <span className="material-icons" style={{ fontSize: '1.5rem' }}>exit_to_app</span>
                                </Nav.Link>
                            </>
                        )}
                    </Nav>
                </BootstrapNavbar.Collapse>
            </Container>
        </BootstrapNavbar>
    );
}

export default ColorSchemesExample;
