import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const PopupListening = ({ isOpen, onClose, questionText }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;

    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button btn-close" aria-label="Close"></button>
                <div>
                    <p>{questionText}</p>

                </div>
            </div>
        </div>

    );
};

export default PopupListening;
