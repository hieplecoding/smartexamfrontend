import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const PopupAkkusativ = ({ isOpen, onClose }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;

    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button">&times;</button>
                <div>
                    <h3>Akkusativ</h3>
                    <p>der -{'>'} den</p>
                    <p>die -{'>'} die</p>
                    <p>das -{'>'} das</p>
                    <p>ich -{'>'} mich</p>
                    <p>du -{'>'} dich</p>
                    <p>er -{'>'} ihn</p>
                    <p>sie -{'>'} sie</p>
                    <p>es -{'>'} es</p>
                    <p>ihr -{'>'} euch</p>
                    <p>wir -{'>'}; uns</p>
                    <p>Sie/sie -{'>'} Sie/sie</p>

                </div>
            </div>
        </div>
    );
};

export default PopupAkkusativ;
