import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module
const PopupGenitiv = ({ isOpen, onClose }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;

    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button">&times;</button>
                <div>
                    <h3>Genitiv</h3>
                    <p>des -{'>'} des</p>
                    <p>der -{'>'} der</p>
                    <p>des -{'>'} des</p>
                    <p>mein -{'>'} meiner/meines</p>
                    <p>dein-{'>'} deiner/deines</p>
                    <p>sein -{'>'} seiner/seines</p>
                    <p>ihr -{'>'} ihrer/ihres</p>
                    <p>sein -{'>'} seiner/seines</p>
                    <p>unser -{'>'} unserer/unseres</p>
                    <p>eure -{'>'} eurer/eures</p>
                    <p>ihre -{'>'} ihrer/ihres</p>
                    <p>Ihre -{'>'} Ihrer/Ihres</p>
                </div>

            </div>
        </div>
    );
};


export default PopupGenitiv;
