import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const RelativSatz = ({ isOpen, onClose }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;
    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button btn-close" aria-label="Close"></button>
                <div>
                    <h3>Relativsatz</h3>
                    <div className="table-responsive-custom"> {/* Custom class for scrollable table */}
                        <table className="table table-striped table-bordered">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Fall/Kasus</th>
                                    <th>der</th>
                                    <th>die</th>
                                    <th>das</th>
                                    <th>die</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nominativ</td>
                                    <td>der</td>
                                    <td>die</td>
                                    <td>das</td>
                                    <td>die</td>
                                </tr>
                                <tr>
                                    <td>Akkusativ</td>
                                    <td>den</td>
                                    <td>die</td>
                                    <td>das</td>
                                    <td>die</td>
                                </tr>
                                <tr>
                                    <td>Dativ</td>
                                    <td>dem</td>
                                    <td>der</td>
                                    <td>dem</td>
                                    <td>denen</td>
                                </tr>
                                <tr>
                                    <td>Genitiv</td>
                                    <td>dessen</td>
                                    <td>deren</td>
                                    <td>dessen</td>
                                    <td>deren</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default RelativSatz;
