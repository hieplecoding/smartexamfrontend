import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module
const PopupDativ = ({ isOpen, onClose }) => {
  useEffect(() => {
    if (isOpen) {
        // Disable sticky navbar when popup opens
        window.toggleNavbarStickiness(false);
    }

    // Re-enable sticky navbar when the popup is closed
    return () => {
        window.toggleNavbarStickiness(true);
    };
}, [isOpen]); // Effect runs only when isOpen changes

if (!isOpen) return null;

  return (
    <div className="popup-overlay">
      <div className="popup-content">
        <button onClick={onClose} className="popup-close-button">&times;</button>
        <div>
            <h3>Dativ</h3>
            <p>der -{'>'} dem</p>
            <p>die -{'>'} der</p>
            <p>das -{'>'} dem</p>
            <p>ich -{'>'} mir</p>
            <p>du -{'>'} dir</p>
            <p>er -{'>'} ihm</p>
            <p>sie -{'>'} ihr</p>
            <p>es -{'>'} ihm</p>
            <p>ihr -{'>'} euch</p>
            <p>wir -{'>'} uns</p>
            <p>Sie/sie -{'>'} Ihnen/ihnen</p>
        </div>
      </div>
    </div>
  );
};


export default PopupDativ;
