import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const PopupNominativ = ({ isOpen, onClose }) => {
    // Effect to toggle navbar stickiness based on the popup state
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;

    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button">&times;</button>
                <div>
                    <h3>Nominativ</h3>
                    <p>der -{'>'} der</p>
                    <p>die -{'>'} die</p>
                    <p>das -{'>'} das</p>
                    <p>ich -{'>'} ich</p>
                    <p>du -{'>'} du</p>
                    <p>er -{'>'} er</p>
                    <p>sie -{'>'} sie</p>
                    <p>es -{'>'} es</p>
                    <p>ihr -{'>'} ihr</p>
                    <p>wir -{'>'} wir</p>
                    <p>Sie/sie -{'>'} Sie/sie</p>
                </div>
            </div>
        </div>
    );
};

export default PopupNominativ;
