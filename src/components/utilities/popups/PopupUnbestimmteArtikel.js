import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const PopupUnbestimmteArtikel = ({ isOpen, onClose }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;
    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button btn-close" aria-label="Close"></button>
                <div>
                    <h3>Unbestimmte Artikel</h3>
                    <div className="table-responsive-custom"> {/* Custom class for scrollable table */}
                        <table className="table table-striped table-bordered">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Fall/Kasus</th>
                                    <th>der</th>
                                    <th>die</th>
                                    <th>das</th>
                                    <th>Plural</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nominativ</td>
                                    <td>ein</td>
                                    <td>eine</td>
                                    <td>ein</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Akkusativ</td>
                                    <td>einen</td>
                                    <td>eine</td>
                                    <td>ein</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Dativ</td>
                                    <td>einem</td>
                                    <td>einer</td>
                                    <td>einem</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Genitiv</td>
                                    <td>eines (+s/es)</td>
                                    <td>der</td>
                                    <td>eines (+s/es)</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default PopupUnbestimmteArtikel;
