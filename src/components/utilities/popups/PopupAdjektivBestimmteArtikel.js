import React, { useEffect } from 'react';
import '../../../App.css'; // Import the CSS module

const PopupAdjektiveBestimmteArtikel = ({ isOpen, onClose }) => {
    useEffect(() => {
        if (isOpen) {
            // Disable sticky navbar when popup opens
            window.toggleNavbarStickiness(false);
        }

        // Re-enable sticky navbar when the popup is closed
        return () => {
            window.toggleNavbarStickiness(true);
        };
    }, [isOpen]); // Effect runs only when isOpen changes

    if (!isOpen) return null;

    return (
        <div className="popup-overlay">
            <div className="popup-content">
                <button onClick={onClose} className="popup-close-button btn-close" aria-label="Close">&times;</button>
                <div>
                    <h3>Adjektive mit bestimmtem Artikel</h3> {/* Title changed to match content */}
                    <div className="table-responsive-custom"> {/* Makes the table scrollable */}
                        <table className="table table-striped table-bordered">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Fall/Kasus</th>
                                    <th>der</th>
                                    <th>die</th>
                                    <th>das</th>
                                    <th>Plural</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nominativ</td>
                                    <td>-e</td>
                                    <td>-e</td>
                                    <td>-e</td>
                                    <td>-en</td>
                                </tr>
                                <tr>
                                    <td>Akkusativ</td>
                                    <td>-en</td>
                                    <td>-e</td>
                                    <td>-e</td>
                                    <td>-en</td>
                                </tr>
                                <tr>
                                    <td>Dativ</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                </tr>
                                <tr>
                                    <td>Genitiv</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                    <td>-en</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PopupAdjektiveBestimmteArtikel;
