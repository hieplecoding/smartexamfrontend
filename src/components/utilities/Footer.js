import React from 'react';
import { Container, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useAuth } from '../Hooks/useAuth';
import '../../css/Footer.css';

const Footer = () => {
  const auth = useAuth();

  return (
    <footer className="footer">
      <Container className="text-center py-3">
        © 2024 Made by Smart German Exam Team with <span style={{ color: 'red' }}>&hearts;</span>
        <div className="footer-content">
          <p>Liên hệ với chúng tôi</p>
          <a href="mailto:smartgermanexam@gmail.com" className="contact-email">smartgermanexam@gmail.com</a>

          {!auth.isAuthenticated && (
            <div className="footer-links">
              <LinkContainer to="/contact">
                <Nav.Link className="footer-link text-nowrap">Liên hệ</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/team-sge">
                <Nav.Link className="footer-link text-nowrap">Về team SGE</Nav.Link>
              </LinkContainer>
            </div>
          )}

          <div className="footer-icons">
            <a href="https://www.facebook.com/profile.php?id=61557140912058" target="_blank" rel="noopener noreferrer">
              <i className="fa-brands fa-facebook"></i>
            </a>
          </div>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
