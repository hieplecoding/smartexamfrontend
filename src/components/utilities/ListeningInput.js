import React, { useState } from 'react';
import PopupListening from './popups/PopupListening';

const ListeningInput = ({ question, onCorrectAnswer, locked }) => {
    const [userAnswer, setUserAnswer] = useState('');
    const [serverResponse, setServerResponse] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    // set popup is false is not opened at the beginning
    const [isPopupOpen, setIsPopupOpen] = useState(false);

    const togglePopup = () => {
        setIsPopupOpen(!isPopupOpen);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        const token = localStorage.getItem('token');
        if (!token) {
            setIsLoading(false);
            // Handle the case where the user is not logged in
            return;
        }
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/listening_question`;
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ id_question: question.id_question, user_answer: userAnswer })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                setIsLoading(false);
                // Check if the response is directly available as a string
                if (typeof data.response === 'string' && data.response === "Es ist richtig") {
                    setServerResponse(data.response);
                    onCorrectAnswer();
                }
                else if (typeof data.response === 'string' && data.response === "Es ist falsch") {
                    setServerResponse(data.response);

                }

                else if (data.response && data.response.content) {
                    setServerResponse(data.response.content);
                } else {
                    console.error('Unexpected response structure:', data);
                    setServerResponse('Unexpected response structure. Please contact support.');
                }
            })

            .catch(error => {
                setIsLoading(false);
                console.error(`Error submitting question ${question.id_question}:`, error.message);
                setServerResponse(`Error: ${error.message}`);
            });

    };
    const handleOptionChange = (changeEvent) => {
        setUserAnswer(changeEvent.target.value);
    };

    return (
        <>
            <div className="container mt-1">
                <div className="card shadow-sm p-3 bg-white rounded" style={{ borderRadius: '25px' }}>
                    <div className="card-body">
                        {question.audio_url && (
                            <audio controls src={question.audio_url} className="w-100 mb-3">
                                Your browser does not support the audio element.
                            </audio>
                        )}
                        <h5 className="card-title">{question.question}</h5>
                        <form onSubmit={handleSubmit}>
                            {['answer_1', 'answer_2', 'answer_3'].map((answerKey) => (
                                <div key={answerKey} className="mb-3 form-check">
                                    <input
                                        className="form-check-input"
                                        type="radio"
                                        name="answer"
                                        id={answerKey}
                                        value={answerKey}
                                        checked={userAnswer === question[answerKey] || userAnswer === answerKey}
                                        onChange={handleOptionChange}
                                        disabled={locked}  // This controls whether the input can be interacted with
                                    />
                                    <label className="form-check-label" htmlFor={answerKey}>
                                        {question[answerKey]}
                                    </label>
                                </div>
                            ))}
                            <button type="submit" className="btn btn-primary mt-2">Trả lời</button>
                        </form>
                        {/* Button styled as a link to toggle the popup */}
                        <div className="col-md-6"> {/* Grid column for the button */}
                            <button onClick={togglePopup} style={{
                                background: 'none',
                                border: 'none',
                                color: '#5B7C99', // Adjusted to match your color preference
                                textDecoration: 'none',
                                display: 'block',
                                marginTop: '10px',
                                cursor: 'pointer',
                                padding: 0,
                                fontSize: '16px', // Example font size, adjust as needed
                                fontWeight: 'bold' // Example font weight, adjust as needed
                            }}>
                                Đoạn hội thoại.
                            </button>
                        </div>

                        {/* Popup component */}
                        <PopupListening isOpen={isPopupOpen} onClose={togglePopup} questionText={question.information_text} />
                    </div>
                </div>
                {isLoading ? (
                    <div className="loader"></div> // Show loading icon when isLoading is true
                ) : serverResponse ? (
                    <div className={`alert ${serverResponse === "Es ist richtig" ? "alert-success" : "alert-warning"}`}>
                        Phản hồi: {serverResponse}
                    </div>
                ) : null}
            </div>
        </>

    );
};

export default ListeningInput;
