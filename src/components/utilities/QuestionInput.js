import React, { useState } from 'react';

const QuestionInput = ({ question, onCorrectAnswer }) => {
    const [userAnswer, setUserAnswer] = useState('');
    const [serverResponse, setServerResponse] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        const token = localStorage.getItem('token');
        if (!token) {
            setIsLoading(false);
            // Handle the case where the user is not logged in
            return;
        }
        const apiUrl = `${process.env.REACT_APP_API_BASE_URL}/smart_multiple_choice`;
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ id_question: question.id_question, user_answer: userAnswer })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                setIsLoading(false);

                if (typeof data.response === 'string' && data.response === "Es ist richtig") {
                    setServerResponse(data.response);
                    onCorrectAnswer();
                }
                else if (data.response && data.response.content) {
                    setServerResponse(data.response.content);
                } else {
                    console.error('Unexpected response structure:', data);
                    setServerResponse('Unexpected response structure. Please contact support.');
                }
            })
            .catch(error => {
                setIsLoading(false);
                console.error(`Error submitting question ${question.id_question}:`, error.message);
                setServerResponse(`Error: ${error.message}`);
            });
    };

    const handleOptionChange = (changeEvent) => {
        setUserAnswer(changeEvent.target.value);
    };

    return (
        <>

            <div className="container mt-5">
                <div className="card shadow-sm p-3 mb-5 bg-white rounded" style={{ borderRadius: '25px' }}>
                    <div className="card-body">
                        <h5 className="card-title">{question.information_text}</h5>
                        <h5 className="card-title">{question.question}</h5>
                        <form onSubmit={handleSubmit}>
                            {['answer_1', 'answer_2', 'answer_3'].map((answerKey) => (
                                <div key={answerKey} className="mb-3 form-check">
                                    <input
                                        className="form-check-input"
                                        type="radio"
                                        name="answer"
                                        id={answerKey}
                                        value={answerKey}
                                        checked={userAnswer === answerKey}
                                        onChange={handleOptionChange}
                                        style={{ borderRadius: '25px' }}
                                    />
                                    <label className="form-check-label" htmlFor={answerKey}>
                                        {question[answerKey]}
                                    </label>
                                </div>
                            ))}
                            <button type="submit" className="btn btn-primary mt-2">Gửi câu hỏi</button>
                        </form>
                    </div>
                </div>
                {isLoading ? (
                    <div className="loader"></div> // Show loading icon when isLoading is true
                ) : serverResponse ? (
                    <div className={`alert ${serverResponse === "Es ist richtig" ? "alert-success" : "alert-warning"}`}>
                        Câu Trả lời: {serverResponse}
                    </div>
                ) : null}
            </div>
        </>

    );
};

export default QuestionInput;
