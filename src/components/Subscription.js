// Subscription.js
import React from 'react';
import { Container, Card } from 'react-bootstrap';
import '../css/Subscription.css'
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';


const Subscription = () => {
    return (
        <Container className="custom-container">
            <h2 style={{
                fontWeight: 'bold', paddingBottom: '50px' // This will apply bold styling to the h2 element
            }}>Những gói dịch vụ</h2>
            <div className="pricing-card-container mb-3 text-center">
                {/* Card 1 */}
                <div className="pricing-card pricingcard-3">
                    <Card>
                        <Card.Header>
                            <h4 className="my-0 font-weight-normal">1 Tháng</h4>
                        </Card.Header>
                        <Card.Body>
                            <Card.Title className="pricing-card-title">
                                150.000 vnd
                            </Card.Title>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>Bạn muốn thử sức với việc học tiếng Đức với cường độ nhẹ nhàng</li>
                                <li>Hỗ trợ 24/7</li>
                                <li>Đề thi phong phú</li>
                            </ul>
                            <div className='button-subscription'>
                                <Link to="/bank-transfer">
                                    <button type="button">
                                        Liên hệ chúng tôi
                                    </button>
                                </Link>
                            </div>
                        </Card.Body>
                    </Card>
                </div>

                {/* Card 2 - Middle Card */}
                <div className="pricing-card middle-card" style={{ width: '200px' }}>
                    <Card>
                        <Card.Header>
                            <h4 className="my-0 font-weight-normal">3 Tháng</h4>
                        </Card.Header>
                        <Card.Body>
                            <Card.Title className="pricing-card-title">
                                382.500 vnd
                            </Card.Title>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>Bạn muốn học tiếng Đức thật sự nghiêm túc</li>
                                <li>Giảm giá</li>
                                <li className="highlight-discount">15 %</li> {/* Custom class for styling */}
                                <li>Sử dụng tất cả dịch vụ</li>
                                <li>Hỗ trợ 24/7</li>
                            </ul>

                            <LinkContainer to="/bank-transfer" style={{ color: 'green' }}>
                                <Button variant="light" className="green-text-button">
                                    Tìm hiểu thêm
                                </Button>
                            </LinkContainer>

                        </Card.Body>
                    </Card>
                </div>

                {/* Card 3 */}
                <div className="pricing-card pricingcard-3">
                    <Card>
                        <Card.Header>
                            <h4 className="my-0 font-weight-normal">6 Tháng</h4>
                        </Card.Header>
                        <Card.Body>
                            <Card.Title className="pricing-card-title">
                                675.000 vnd
                            </Card.Title>
                            <ul className="list-unstyled mt-3 mb-4">
                                <li>Bạn muốn đạt được chứng chỉ như A2 hoặc B1</li>
                                <li>Giảm giá đến 25%</li>
                                <li>Sử dụng tất cả dịch vụ</li>
                                <li>Hỗ trợ 24/7</li>
                            </ul>
                            <div className='button-subscription'>
                                <Link to="/bank-transfer">
                                    <button type="button">
                                        Liên hệ chúng tôi
                                    </button>
                                </Link>
                            </div>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </Container>
    );
};

export default Subscription;
