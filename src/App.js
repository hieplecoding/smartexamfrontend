import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import SmartCheckingEssay from './components/SmartCheckingEssay';
import Navbar from './components/utilities/Navbar';
import Footer from './components/utilities/Footer';
import LandingPage from './components/LandingPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useAuth } from './components/Hooks/useAuth';
import SmartListening from './components/SmartListening';
import SmartGrammarQuestion from './components/SmartGrammarQuestion';
import LandingPage2 from './components/LandingPage2';
import UserProfile from './components/UserProfile';
import VerifyEmail from './components/VerifyEmail';
import ChangePassword from './components/ChangePassword';
import ExpiredSubscription from './components/ExpiredSubscription';
import BankTransfer from './components/BankTransfer';
import HandleVerify from './components/HandleVerify';
import ComingSoon from './components/ComingSoon';
import ContactPage from './components/ContactPage';
import SmartSpeaking from './components/Services/Speaking/SmartSpeaking';
import TeamPageWrapper from './components/TeamPageWrapper';

function App() {
  const auth = useAuth();
  return (
    <>
      <div className='content-wrap'>
        <Navbar />
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/verify-email" element={<VerifyEmail />} /> 
          <Route path="/landing-2"
            element={auth.isAuthenticated ? <LandingPage2 /> : <Navigate to="/login" />} />
          <Route path="/smart-checking-essay"
            element={auth.isAuthenticated ? <SmartCheckingEssay /> : <Navigate to="/login" />} />
          <Route path="/smart-listening-question"
            element={auth.isAuthenticated ? <SmartListening /> : <Navigate to="/login" />} />
          <Route path="/grammar-question"
            element={auth.isAuthenticated ? <SmartGrammarQuestion /> : <Navigate to="/login" />} />   
          <Route path="/user-profile"
            element={auth.isAuthenticated ? <UserProfile /> : <Navigate to="/login" />} />   
          <Route path="/change-password" element={<ChangePassword />} />
          <Route path="/listening-level" element={<SmartListening />} />
          <Route path="/expired-subscription" element={<ExpiredSubscription />} />
          <Route path="/bank-transfer" element={<BankTransfer />} />
          <Route path="/handle-verify" element={<HandleVerify />} />
          <Route path="/coming-soon" element={<ComingSoon />} />
          <Route path="/team-sge" element={<TeamPageWrapper />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="/conversation" element={<SmartSpeaking />} />
        </Routes>
      </div>
      <Footer />
    </>
  );
}

export default App;
