# Step 1: Use an official Node image as a parent image
FROM node:latest

# Step 2: Set the working directory
WORKDIR /app

# Step 3: Copy package.json and package-lock.json (or yarn.lock)
COPY package*.json ./

# Step 4: Install dependencies
RUN npm install

# Step 5: Copy the rest of your app's source code
COPY . .

# Step 6: Build your app
RUN npm run build

# Step 7: Install serve to serve the static files
RUN npm install -g serve

# Step 8: Define the command to run your app
CMD ["serve", "-s", "build", "--single"]
